﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using YouAreBaba.Model;
using YouAreBaba.Repositorys;

namespace YouAreBaba.Logic
{
    public class MenuLogic : IMenuLogic
    {

        private IRepository repository;
        private IDatabaseRepository repositoryDB;

        public MenuLogic()
        {
            this.repository = new Repository();
            this.repositoryDB = new DatabaseRepository();
        }
        public void CleanLastPlayedMap(string name)
        {
            this.repositoryDB.DeleteAllRecord(name);
        }
        public List<int[]> ScanMapSizes()
        {
            return this.repository.ScanMapSizes();
        }

        // HIGHSCORE ******************************************************
        public List<Highscore> Get()
        {
            return this.repositoryDB.Get();
        }

        public bool Add(string name, int mapid, int time)
        {
            return this.repositoryDB.Add(name, mapid, time);
        }

        public bool DeleteAllRecord(string name)
        {
            return this.repositoryDB.DeleteAllRecord(name);
        }
        public List<int> GetAllVictoryMapNumberByUser(string name)
        {
            var outpu = new List<int>();
            var listofusers = this.Get();
            foreach(var x in listofusers)
            {
                if (x.Name.Equals(name))
                {
                    outpu.Add(x.Mapid);
                }
            }
            return outpu;
        }
        public bool CheckUserExists(string name)
        {
            var listofusers = this.Get();
            foreach (var x in listofusers)
            {
                if (x.Name.Equals(name))
                {
                    return true;
                }
            }
            return false;
        }
        public void ChangeLastPlayedMapButtonToRed(FluidButton be)
        {
            be.Background = new SolidColorBrush(Colors.Red);
        }
        public void ChangeLastPlayedMapButtonToGreen(FluidButton be)
        {
            be.Background = new SolidColorBrush(Colors.Green);
        }
        public void ChangeMapButtonToGreen(FluidButton be)
        {
            be.Background = new SolidColorBrush(Colors.Green);
        }

        public bool CanThisMapBePlayedByUser(string username, int mapid)
        {
            var temp = this.GetAllVictoryMapNumberByUser(username);
            if (temp.Count() == 0) temp.Add(0);
            else temp.Add(temp.Max() + 1);
            if (temp.Contains(mapid)) return true;
            else return false;
        }
        // HIGHSCORE ******************************************************
    }
}
