﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouAreBaba.Model;
using YouAreBaba.Repositorys;

namespace YouAreBaba.Logic
{
    public class UserLogic : IUserLogic
    {
        private IDatabaseRepository repository;
        public UserLogic()
        {
            this.repository = new DatabaseRepository();
        }
        private List<Highscore> Get()
        {
            return this.repository.Get();
        }
        public List<string> GetOnlyNmes()
        {
            var tem = this.repository.Get();
            var liter = new List<string>();
            foreach(var x in tem)
            {
                if (!liter.Contains(x.Name))
                {
                    liter.Add(x.Name);
                }
            }
            return liter;
        }
        public bool CheckUserExists(string name)
        {
            var listofusers = this.Get();
            foreach (var x in listofusers)
            {
                if (x.Name.Equals(name))
                {
                    return true;
                }
            }
            return false;
        }
        public bool Add(string name, int mapid, int time)
        {

            return this.repository.Add(name, mapid, time);
        }
    }
}
