﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Windows;
using YouAreBaba.Model;
using YouAreBaba.Repositorys;

namespace YouAreBaba.Logic
{
    public class GameLogic : IGameLogic
    {
        private int width;
        private int height;
        private int ObjectPixel;
        private IRepository repository;
        private MapObjectEnum[] OperatorEnumList;
        private MapObjectEnum[] PropertiesEnumList;
        private MapObjectEnum[] WORDEnumList;
        private MapObjectEnum[] NotWordEnumList;
        private List<RuleObject> currentrulesofthegame;
        public List<RuleObject> Currentrulesofthegame { get; }

        private List<MapObject> PreviousStateOfcpm { set; get; }
        List<int> theseshouldnotmove;
        List<int> onlymoveone;

        public GameLogic(int width, int height, int ObjectPixel)
        {
            this.width = width; this.height = height; this.ObjectPixel = ObjectPixel;
            this.repository = new Repository(this.ObjectPixel, width, height);
            this.OperatorEnumList = new MapObjectEnum[] { MapObjectEnum.IS /*, MapObjectEnum.HAS, MapObjectEnum.AND,  MapObjectEnum.MAKE, MapObjectEnum.ON, MapObjectEnum.NEAR */ };
            this.PropertiesEnumList = new MapObjectEnum[] { MapObjectEnum.YOU, MapObjectEnum.WIN, MapObjectEnum.PUSH, /* MapObjectEnum.PULL, */ MapObjectEnum.STOP, MapObjectEnum.SINK, MapObjectEnum.DEFEAT, MapObjectEnum.MOVE, MapObjectEnum.SHUT, MapObjectEnum.OPEN, MapObjectEnum.WEAK,  MapObjectEnum.SWAP };
            this.WORDEnumList = new MapObjectEnum[] { MapObjectEnum.BABA_WORD, MapObjectEnum.EMPTY_WORD, MapObjectEnum.WALL_WORD, MapObjectEnum.WATER_WORD, MapObjectEnum.SKULL_WORD, MapObjectEnum.FENCE_WORD, MapObjectEnum.HEDGE_WORD, MapObjectEnum.TREE_WORD, MapObjectEnum.FLAG_WORD, MapObjectEnum.KEY_WORD, MapObjectEnum.GRASS_WORD, MapObjectEnum.KEKE_WORD, MapObjectEnum.ICE_WORD, MapObjectEnum.BELT_WORD, MapObjectEnum.BOX_WORD, MapObjectEnum.LEVEL_WORD, MapObjectEnum.LAVA_WORD, MapObjectEnum.DOOR_WORD, MapObjectEnum.ROCK_WORD };
            this.NotWordEnumList = new MapObjectEnum[] { MapObjectEnum.BABA, MapObjectEnum.EMPTY, MapObjectEnum.WALL, MapObjectEnum.WATER, MapObjectEnum.SKULL, MapObjectEnum.FENCE, MapObjectEnum.HEDGE, MapObjectEnum.TREE, MapObjectEnum.FLAG, MapObjectEnum.KEY, MapObjectEnum.GRASS, MapObjectEnum.KEKE, MapObjectEnum.ICE, MapObjectEnum.BELT, MapObjectEnum.BOX, /* MapObjectEnum.LEVEL, */ MapObjectEnum.LAVA, MapObjectEnum.DOOR, MapObjectEnum.ROCK };
            this.currentrulesofthegame = new List<RuleObject>();
            this.theseshouldnotmove = new List<int>();
            this.onlymoveone = new List<int>();
        }
        public void MoveSmoothlyPixels(List<MapObject> cpm, int x, int y, int thismuchmoved)
        {
            for(int i=0; i< cpm.Count();i++)
            {
                if (
                    (
                    (cpm[i].RelevantBehaviors.Contains(MapObjectEnum.YOU) 
                    || cpm[i].RelevantBehaviors.Contains(MapObjectEnum.MOVE)
                    )
                    && !cpm[i].RelevantBehaviors.Contains(MapObjectEnum.NEEDTOMOVE)
                    )
                    || cpm[i].RelevantBehaviors.Contains(MapObjectEnum.ONLYMOVEONCE)
                    )
                {
                    if (thismuchmoved == 1) cpm[i].Move(- x * this.ObjectPixel,- y * this.ObjectPixel);
                    if (y == 0)
                    {
                        if (cpm[i].Area.X >= PreviousStateOfcpm[i].Area.X)
                        {
                            cpm[i].Move(x, y);
                            cpm[i].Move(x, y);
                        }
                        else if (cpm[i].Area.X <= PreviousStateOfcpm[i].Area.X)
                        {
                            cpm[i].Move(-x, y);
                            cpm[i].Move(-x, y);
                        }
                    }
                    else if (x == 0)
                    {
                        if (cpm[i].Area.Y >= PreviousStateOfcpm[i].Area.Y)
                        {
                            cpm[i].Move(x, y);
                            cpm[i].Move(x, y);
                        }
                        else if (cpm[i].Area.Y <= PreviousStateOfcpm[i].Area.Y)
                        {
                            cpm[i].Move(x, -y);
                            cpm[i].Move(x, -y);
                        }
                    } 
                }
            }
        }

        public int MoveEveryMovableObject(List<MapObject> cpm, int x, int y)
        {
            // Kiürítjük 
            this.onlymoveone.Clear();
            this.theseshouldnotmove.Clear();

            // -2 eltároljuk honnan lépünk a smooth mozgáshoz

            this.PreviousStateOfcpm = cpm;

            // -1. A szabályok mindig PUSH
            // 0. A külső két sor mindig keret!,

            foreach (var tree_check in cpm)
            {
                if(tree_check.MyProperty.Equals(MapObjectEnum.KERET)) tree_check.RelevantBehaviors.Add(MapObjectEnum.STOP);
                if(this.WORDEnumList.Contains(tree_check.MyProperty) || this.PropertiesEnumList.Contains(tree_check.MyProperty) || this.OperatorEnumList.Contains(tree_check.MyProperty)) tree_check.RelevantBehaviors.Add(MapObjectEnum.PUSH);
            }

            // 1. Megkeressük és eltároljuk az összes szabályt //
            for (int i = 0; i < cpm.Count - 1; i++)
            {
                if (OperatorEnumList.Contains(cpm[i].MyProperty))
                {
                    if (i > 0 && i < (this.height - 1) * this.width - 2)
                    {
                        if (WORDEnumList.Contains(cpm[i - 1].MyProperty) && PropertiesEnumList.Contains(cpm[i + 1].MyProperty))
                        {
                            var temper = new RuleObject(cpm[i - 1].MyProperty, cpm[i].MyProperty, cpm[i + 1].MyProperty, VerticalHorizontalEnum.HORIZONTAL);
                            Console.WriteLine("Vertikális Szabály hozzáadva: " + temper.WORD + " " + temper.Operator + " " + temper.Property);
                            currentrulesofthegame.Add(temper);
                        }
                    }
                    if (i > (this.width - 1) && i < (this.height - 1) * this.width)
                    {
                        if (WORDEnumList.Contains(cpm[i - this.width].MyProperty) && PropertiesEnumList.Contains(cpm[i + this.width].MyProperty))
                        {
                            var temper = new RuleObject(cpm[i - this.width].MyProperty, cpm[i].MyProperty, cpm[i + this.width].MyProperty, VerticalHorizontalEnum.VERTICAL);
                            Console.WriteLine("Függőleges Szabály hozzáadva: " + temper.WORD + " " + temper.Operator + " " + temper.Property);
                            currentrulesofthegame.Add(temper);
                        }
                    }
                }
            }

            // 2. A MapObjectek RelevantBehaviors-ába elrakjuk, hogy rá most, ebben a körben vonatkozik ez a szabály // 
            foreach (var currentobject in cpm) if (this.NotWordEnumList.Contains(currentobject.MyProperty)) foreach (var rule in currentrulesofthegame) if (this.WORDtoObject(rule.WORD).Equals(currentobject.MyProperty)) currentobject.RelevantBehaviors.Add(rule.Property);

            // 2.5 Minden amin nincs STOP azon át lehet ugrani
            foreach (var obj in cpm) if(obj.RelevantBehaviors.Count == 0 && !obj.MyProperty.Equals(MapObjectEnum.EMPTY) && !this.OperatorEnumList.Contains(obj.MyProperty) && !this.PropertiesEnumList.Contains(obj.MyProperty) && !this.WORDEnumList.Contains(obj.MyProperty)) obj.RelevantBehaviors.Add(MapObjectEnum.WEAK);

            // 3. Élünk-e még // 
            int howmanyofyoualiveyet = 0;
            foreach (var mek in cpm) foreach (var tex in mek.RelevantBehaviors) if (tex.Equals(MapObjectEnum.YOU)) howmanyofyoualiveyet++;
            if (howmanyofyoualiveyet < 1) return this.DefeatOnThisMap(cpm);

            // 4. Mozgatunk //
            int indexofcpmnext = 0; int indexofcpmafternext = 0; List<int> theseareremovablebehaviorindexes = new List<int>();
            for (int i = 0; i < cpm.Count; i++)
            {
                if (!cpm[i].MovedInThisTick)
                {
                    // Szépen meghatározzuk a mozgási irányban lévő kövi 2 MapObjectet (x vagy y irány)
                    indexofcpmnext = 0; indexofcpmafternext = 0;
                    if (x == 0 && i > 2 * (this.width - 1) && i < (this.height - 2) * this.width) { indexofcpmnext = i + (y * this.width); indexofcpmafternext = i + (y * this.width) + (y * this.width); }
                    else if (y == 0 && i > 1 && i < this.height * this.width - 2) { indexofcpmnext = i + x; indexofcpmafternext = i + x + x; }

                    // ugye swappoljuk mert a mozgó elem SWAPja bármit swappolhat
                    if(cpm[i].RelevantBehaviors.Contains(MapObjectEnum.SWAP) && !cpm[indexofcpmnext].MyProperty.Equals(MapObjectEnum.EMPTY)) cpm[indexofcpmnext].RelevantBehaviors.Add(MapObjectEnum.SWAP);
                    // A WIN-t mindig a végére pakoljuk
                    if (cpm[indexofcpmnext].RelevantBehaviors.Contains(MapObjectEnum.SWAP) && cpm[indexofcpmnext].RelevantBehaviors.Contains(MapObjectEnum.WIN)) cpm[indexofcpmnext].RelevantBehaviors.Remove(MapObjectEnum.WIN);
                    // Az adott Mapobjecten szépen végig kell mennünk, hogy rá melyik szabályokat kell alkalmaznunk
                    if (cpm[i].RelevantBehaviors.Count > 0)
                    {
                        foreach (var rule in cpm[i].RelevantBehaviors)
                        {
                            // csak azoknál a MapObjecteknél kell kezdeményezni mozgást, amelyek vagy te vagy vagy mozgató tulajdonsággal rendelkeznek
                            if (rule.Equals(MapObjectEnum.YOU) || rule.Equals(MapObjectEnum.MOVE))
                            {
                                // Lekezeljük amikor üres helyre lép át az objektum, hisz így nincsenek szabályok
                                if (cpm[indexofcpmnext].MyProperty.Equals(MapObjectEnum.EMPTY))
                                {
                                    if (y == 0) { var imp = cpm[i].Dx; cpm[i].Move(x * this.ObjectPixel, 0); cpm[indexofcpmnext].Move(-1 * x * this.ObjectPixel, 0); }
                                    else if (x == 0) { var imp = cpm[i].Dy; cpm[i].Move(0, y * this.ObjectPixel); cpm[indexofcpmnext].Move(0, -1 * y * this.ObjectPixel); }
                                    var temp = cpm[indexofcpmnext]; cpm[indexofcpmnext] = cpm[i]; cpm[i] = temp; cpm[indexofcpmnext].MoveInThisTick();
                                }
                                // Ha egyik se volt a tömb, akkor végignézzük a szabályokat
                                else
                                {
                                    if (cpm[indexofcpmnext].RelevantBehaviors.Count < 1) break;
                                    foreach (var nexttorule in cpm[indexofcpmnext].RelevantBehaviors)
                                    {
                                        switch (nexttorule)
                                        {
                                            case MapObjectEnum.STOP:
                                                this.theseshouldnotmove.Add(i);
                                                // semmi nem történik, hiszen nem mozdulhatsz ide! 
                                                // Nem kéne ideírni de így nem fut feleslegesen a switch case.
                                                break;
                                            case MapObjectEnum.SHUT:
                                                this.theseshouldnotmove.Add(i);
                                                // semmi nem történik, hiszen nem mozdulhatsz ide! 
                                                // Nem kéne ideírni de így nem fut feleslegesen a switch case.
                                                break;
                                            case MapObjectEnum.PUSH:
                                                PushHelper(cpm, x, y, i, indexofcpmnext, indexofcpmafternext);
                                                if (cpm[indexofcpmnext].MyProperty.Equals(MapObjectEnum.EMPTY))
                                                {
                                                    if (y == 0)
                                                    {
                                                        cpm[indexofcpmnext].Move(-1 * x * this.ObjectPixel, 0);
                                                        cpm[i].Move(x * this.ObjectPixel, 0);
                                                    }
                                                    else if (x == 0)
                                                    {
                                                        cpm[indexofcpmnext].Move(0, -1 * y * this.ObjectPixel);
                                                        cpm[i].Move(0, y * this.ObjectPixel);
                                                    }
                                                    var tempnext = cpm[indexofcpmnext];
                                                    cpm[indexofcpmnext] = cpm[i];
                                                    cpm[i] = tempnext;
                                                    cpm[indexofcpmnext].MovedInThisTick = true;
                                                }
                                                else
                                                {
                                                    //this.theseshouldnotmove.Add(i);
                                                    if (y == 0)
                                                    {
                                                        for(int gi = i; !cpm[gi].MyProperty.Equals(MapObjectEnum.KERET); gi += x)
                                                        {
                                                            this.onlymoveone.Remove(gi);
                                                            this.theseshouldnotmove.Add(gi);
                                                        }

                                                    }
                                                    else if (x == 0)
                                                    {
                                                        for (int gi = i; !cpm[gi].MyProperty.Equals(MapObjectEnum.KERET); gi += y * this.width)
                                                        {
                                                            this.onlymoveone.Remove(gi);
                                                            this.theseshouldnotmove.Add(gi);
                                                        }
                                                    }
                                                }
                                                break;
                                            case MapObjectEnum.SINK:
                                                cpm[i].MyProperty = MapObjectEnum.EMPTY;
                                                theseareremovablebehaviorindexes.Add(i);
                                                break;
                                            case MapObjectEnum.DEFEAT:
                                                cpm[i].MyProperty = MapObjectEnum.EMPTY;
                                                theseareremovablebehaviorindexes.Add(i);
                                                howmanyofyoualiveyet--;
                                                if (howmanyofyoualiveyet < 1) return this.DefeatOnThisMap(cpm);
                                                break;
                                            case MapObjectEnum.SWAP:
                                                if (y == 0)
                                                {
                                                    cpm[i].Move(x * this.ObjectPixel, 0);
                                                    cpm[indexofcpmnext].Move(-1 * x * this.ObjectPixel, 0);
                                                }
                                                else if (x == 0)
                                                {
                                                    cpm[i].Move(0, y * this.ObjectPixel);
                                                    cpm[indexofcpmnext].Move(0, -1 * y * this.ObjectPixel);
                                                }
                                                var temper = cpm[indexofcpmnext];
                                                cpm[indexofcpmnext] = cpm[i];
                                                cpm[i] = temper;
                                                cpm[i].MovedInThisTick = true;
                                                cpm[indexofcpmnext].MovedInThisTick = true;
                                                break;
                                            case MapObjectEnum.WEAK:
                                                var foundempty = i;
                                                if (y==0) foundempty = JumpHelper(cpm, x, y, i + x);
                                                else if (x==0) foundempty = JumpHelper(cpm, x, y, i + y * this.width);
                                                if (foundempty != -1)
                                                {
                                                    if (y == 0) { cpm[i].Move((foundempty - i) * this.ObjectPixel, 0); cpm[foundempty].Move((-1 * (foundempty - i))  * this.ObjectPixel, 0); }
                                                    else if (x == 0) { cpm[i].Move(0, (foundempty - i) / this.width * this.ObjectPixel); cpm[foundempty].Move(0, (-1 * (foundempty - i)) / this.width * this.ObjectPixel); }
                                                    var temp = cpm[foundempty]; cpm[foundempty] = cpm[i]; cpm[i] = temp; cpm[foundempty].MoveInThisTick();
                                                    for (int ij = foundempty; ij > i; ij--) cpm[ij].MoveInThisTick();
                                                }
                                                else
                                                {
                                                    this.theseshouldnotmove.Add(i);
                                                }
                                                break;
                                            case MapObjectEnum.WIN: return this.VictoryOnThisMap();
                                            default:
                                                this.theseshouldnotmove.Add(i);
                                                // Ez lenne amikor rámáshaz a BABA, mert nincs rajta semmi tulajdonság
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    // A játék közben fellépő viselkedési törléseket (SINK, DEFEAT) kell itt lekezelni. A végén persze minden behaviour törlésre kerül, hoyg az ú Move esetén töltődjön.
                    foreach (var xe in theseareremovablebehaviorindexes) cpm[xe].RelevantBehaviors.Clear();
                }
            }
            // szépen vissza kell állítani hogy kövi tick alatt mozoghassanak
            foreach (var xe in cpm) xe.NotMoveInThisTick();
            // 4. Ürítjük amit kell //
            currentrulesofthegame.Clear();
            // 5. a pixelmozgatáshoz cpm-be pakoljuk a NEEDTOMOVE-t
            foreach (var xe in this.theseshouldnotmove) if(!cpm[xe].MyProperty.Equals(MapObjectEnum.KERET)) cpm[xe].RelevantBehaviors.Add(MapObjectEnum.NEEDTOMOVE);
            foreach (var xe in this.onlymoveone) if (!cpm[xe].MyProperty.Equals(MapObjectEnum.KERET)) cpm[xe].RelevantBehaviors.Add(MapObjectEnum.ONLYMOVEONCE);
            return 0;
        }

        private int JumpHelper(List<MapObject> cpm, int x, int y, int i)
        {
            if (cpm[i].MyProperty.Equals(MapObjectEnum.EMPTY)) return i;
            else if (cpm[i].MyProperty.Equals(MapObjectEnum.KERET) || this.WORDEnumList.Contains(cpm[i].MyProperty) || this.PropertiesEnumList.Contains(cpm[i].MyProperty) || this.OperatorEnumList.Contains(cpm[i].MyProperty)) return -1;
            else
            {
                if (y == 0)
                {
                    return JumpHelper(cpm, x, y, i + x);
                }
                else
                {
                    return JumpHelper(cpm, x, y, i + y * this.width);
                }
            }
        }

        private void PushHelper(List<MapObject> cpm, int x, int y, int i, int indexofcpmnext, int indexofcpmafternext)
        {
            if (cpm[i].RelevantBehaviors.Contains(MapObjectEnum.OPEN)
               && (cpm[indexofcpmnext].MyProperty.Equals(MapObjectEnum.DOOR)
               || cpm[indexofcpmnext].MyProperty.Equals(MapObjectEnum.WATER)
               ))
            {
                cpm[i].MyProperty = MapObjectEnum.EMPTY;
                cpm[indexofcpmnext].MyProperty = MapObjectEnum.EMPTY;
            }
            if (cpm[indexofcpmnext].RelevantBehaviors.Contains(MapObjectEnum.OPEN)
               && (cpm[indexofcpmafternext].MyProperty.Equals(MapObjectEnum.DOOR)
               || cpm[indexofcpmafternext].MyProperty.Equals(MapObjectEnum.WATER)
               ))
            {
                cpm[indexofcpmnext].MyProperty = MapObjectEnum.EMPTY;
                cpm[indexofcpmafternext].MyProperty = MapObjectEnum.EMPTY;
            }
            if (cpm[indexofcpmafternext].RelevantBehaviors.Contains(MapObjectEnum.PUSH) || this.WORDEnumList.Contains(cpm[indexofcpmnext].MyProperty) || this.PropertiesEnumList.Contains(cpm[indexofcpmnext].MyProperty) || this.OperatorEnumList.Contains(cpm[indexofcpmnext].MyProperty))
            {
                onlymoveone.Add(indexofcpmafternext);
                if (y == 0)
                {
                    PushHelper(cpm, x, y, i + x, indexofcpmnext + x, indexofcpmafternext + x);
                }
                else if (x == 0)
                {
                    PushHelper(cpm, x, y, i + y * this.width, indexofcpmnext + y * this.width, indexofcpmafternext + y * this.width);
                }
            }
            if (cpm[indexofcpmnext].RelevantBehaviors.Contains(MapObjectEnum.PUSH) && cpm[indexofcpmafternext].RelevantBehaviors.Contains(MapObjectEnum.SINK))
            {
                cpm[indexofcpmnext].MyProperty = MapObjectEnum.EMPTY;
                cpm[indexofcpmafternext].MyProperty = MapObjectEnum.EMPTY;
            }
            if (!cpm[i].MyProperty.Equals(MapObjectEnum.EMPTY) && cpm[indexofcpmnext].RelevantBehaviors.Contains(MapObjectEnum.PUSH) && cpm[indexofcpmafternext].MyProperty.Equals(MapObjectEnum.EMPTY))
            {
                if (y == 0)
                {
                    cpm[indexofcpmafternext].Move(-1 * x * this.ObjectPixel, 0);
                    cpm[indexofcpmnext].Move(x * this.ObjectPixel, 0);
                }
                else if (x == 0)
                {
                    cpm[indexofcpmafternext].Move(0, -1 * y * this.ObjectPixel);
                    cpm[indexofcpmnext].Move(0, y * this.ObjectPixel);
                }
                var tempnext = cpm[indexofcpmafternext];
                cpm[indexofcpmafternext] = cpm[indexofcpmnext];
                cpm[indexofcpmnext] = tempnext;
                cpm[indexofcpmafternext].MovedInThisTick = true;
                onlymoveone.Add(indexofcpmafternext);
            }
        }

        private MapObjectEnum WORDtoObject(MapObjectEnum wORD)
        {
            switch (wORD)
            {
                case MapObjectEnum.BABA_WORD: return MapObjectEnum.BABA;
                case MapObjectEnum.EMPTY_WORD: return MapObjectEnum.EMPTY;
                case MapObjectEnum.WALL_WORD: return MapObjectEnum.WALL;
                case MapObjectEnum.WATER_WORD: return MapObjectEnum.WATER;
                case MapObjectEnum.SKULL_WORD: return MapObjectEnum.SKULL;
                case MapObjectEnum.FENCE_WORD: return MapObjectEnum.FENCE;
                case MapObjectEnum.HEDGE_WORD: return MapObjectEnum.HEDGE;
                case MapObjectEnum.TREE_WORD: return MapObjectEnum.TREE;
                case MapObjectEnum.FLAG_WORD: return MapObjectEnum.FLAG;
                case MapObjectEnum.KEY_WORD: return MapObjectEnum.KEY;
                case MapObjectEnum.GRASS_WORD: return MapObjectEnum.GRASS;
                case MapObjectEnum.KEKE_WORD: return MapObjectEnum.KEKE;
                case MapObjectEnum.ICE_WORD: return MapObjectEnum.ICE;
                case MapObjectEnum.BELT_WORD: return MapObjectEnum.BELT;
                case MapObjectEnum.BOX_WORD: return MapObjectEnum.BOX;
                case MapObjectEnum.LEVEL_WORD: return MapObjectEnum.LEVEL_WORD;
                case MapObjectEnum.LAVA_WORD: return MapObjectEnum.LAVA;
                case MapObjectEnum.DOOR_WORD: return MapObjectEnum.DOOR;
                case MapObjectEnum.ROCK_WORD: return MapObjectEnum.ROCK;
            }
            return wORD;
        }

        private int DefeatOnThisMap(List<MapObject> cpm)
        {
            return 1;
            // Irány vissza a menübe! Nem mentünk vereséget, hisza  map megcnjyitása előtti állapot az érvényes;
        }

        public int VictoryOnThisMap()
        {
            return 2;
            // Majd visszadob a pályaválasztó menübe, miközben elment minden releváns infót;
        }

        public string[,] NewTickSoFreshMapMaking(List<MapObject> currentlyPlayedMap)
        {
            var kimenet = new string[this.height, this.width];
            foreach (var viewedmapobject in currentlyPlayedMap)
            {
                kimenet[viewedmapobject.Dy / ObjectPixel, viewedmapobject.Dx / ObjectPixel] = Enum.GetName(typeof(MapObjectEnum), viewedmapobject.MyProperty);
            }
            return kimenet;
        }

        public List<string> ScanAllFramesOfGif(string temp)
        {
            return repository.ScanAllFramesOfGif(temp);
        }

        public List<List<MapObject>> ScanAllMaps()
        {
            return repository.ScanAllMaps();
        }

        public List<string> ScanAllPictureNames()
        {
            return repository.ScanAllPictureNames();
        }

        public void AddSaveGame(int currentMap, List<MapObject> currentlyPlayedMap)
        {
            this.repository.AddSaveGame(currentMap, currentlyPlayedMap);
        }

        public List<MapObject> LoadGame()
        {
            return this.repository.LoadGame();
        }
    }
}
