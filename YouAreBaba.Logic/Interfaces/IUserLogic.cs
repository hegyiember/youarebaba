﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouAreBaba.Logic
{
    public interface IUserLogic
    {
        bool CheckUserExists(string name);
        bool Add(string name, int mapid, int time);
        List<string> GetOnlyNmes();
    }
}
