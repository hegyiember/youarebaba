﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouAreBaba.Model;

namespace YouAreBaba.Logic
{
    public interface IMenuLogic
    {
        List<int[]> ScanMapSizes();

        // HIGHSCORE ******************************************************
        List<Highscore> Get();
        void CleanLastPlayedMap(string name);

        bool Add(string name, int mapid, int time);

        bool DeleteAllRecord(string name);
        List<int> GetAllVictoryMapNumberByUser(string name);
        bool CheckUserExists(string name);
        void ChangeLastPlayedMapButtonToRed(FluidButton be);
        void ChangeLastPlayedMapButtonToGreen(FluidButton be);
        void ChangeMapButtonToGreen(FluidButton be);
        bool CanThisMapBePlayedByUser(string username, int mapid);
        // HIGHSCORE ******************************************************
    }
}
