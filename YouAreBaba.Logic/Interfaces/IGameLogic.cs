﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouAreBaba.Model;

namespace YouAreBaba.Logic
{
    public interface IGameLogic
    {
        void AddSaveGame(int currentMap, List<MapObject> currentlyPlayedMap);
        int MoveEveryMovableObject(List<MapObject> currentlyPlayedMap, int v1, int v2);
        void MoveSmoothlyPixels(List<MapObject> cpm, int x, int y, int thismuchmoved);
        //string[,] NewTickSoFreshMapMaking(List<MapObject> currentlyPlayedMap);
        List<string> ScanAllPictureNames();
        List<List<MapObject>> ScanAllMaps();
        List<string> ScanAllFramesOfGif(string temp);
        int VictoryOnThisMap();
        List<MapObject> LoadGame();
    }
}
