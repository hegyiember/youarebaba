﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouAreBaba.Model;
using YouAreBaba.Repositorys;

namespace YouAreBaba.Logic
{
    public class GameLogic : IGameLogic
    {
        private readonly Random random = new Random();

        private int width;
        private int height;
        private int ObjectPixel;

        private IRepository repository;

        private MapObjectEnum[] OperatorEnumList;
        private MapObjectEnum[] PropertiesEnumList;
        private MapObjectEnum[] WORDEnumList;
        private MapObjectEnum[] NotWordEnumList;
        private List<RuleObject> currentrulesofthegame;

        public GameLogic(int width, int height, int ObjectPixel)
        {
            this.width = width;
            this.height = height;
            this.ObjectPixel = ObjectPixel;
            this.repository = new Repository(this.ObjectPixel, width, height);
            this.OperatorEnumList = new MapObjectEnum[] { MapObjectEnum.IS, MapObjectEnum.HAS, /*MapObjectEnum.AND, */ MapObjectEnum.MAKE, MapObjectEnum.ON, MapObjectEnum.NEAR };
            this.PropertiesEnumList = new MapObjectEnum[] { MapObjectEnum.YOU, MapObjectEnum.WIN, MapObjectEnum.PUSH, /* MapObjectEnum.PULL, */ MapObjectEnum.STOP, MapObjectEnum.SINK, MapObjectEnum.DEFEAT, MapObjectEnum.MOVE, MapObjectEnum.SHUT, MapObjectEnum.OPEN, /* MapObjectEnum.WEAK, */ MapObjectEnum.SWAP };
            this.WORDEnumList = new MapObjectEnum[] { MapObjectEnum.BABA_WORD, MapObjectEnum.EMPTY_WORD, MapObjectEnum.WALL_WORD, MapObjectEnum.WATER_WORD, MapObjectEnum.SKULL_WORD, MapObjectEnum.FENCE_WORD, MapObjectEnum.HEDGE_WORD, MapObjectEnum.TREE_WORD, MapObjectEnum.FLAG_WORD, MapObjectEnum.KEY_WORD, MapObjectEnum.GRASS_WORD, MapObjectEnum.KEKE_WORD, MapObjectEnum.ICE_WORD, MapObjectEnum.BELT_WORD, MapObjectEnum.BOX_WORD, MapObjectEnum.LEVEL_WORD, MapObjectEnum.LAVA_WORD, MapObjectEnum.DOOR_WORD, MapObjectEnum.ROCK_WORD };
            this.NotWordEnumList = new MapObjectEnum[] { MapObjectEnum.BABA, MapObjectEnum.EMPTY, MapObjectEnum.WALL, MapObjectEnum.WATER, MapObjectEnum.SKULL, MapObjectEnum.FENCE, MapObjectEnum.HEDGE, MapObjectEnum.TREE, MapObjectEnum.FLAG, MapObjectEnum.KEY, MapObjectEnum.GRASS, MapObjectEnum.KEKE, MapObjectEnum.ICE, MapObjectEnum.BELT, MapObjectEnum.BOX, /* MapObjectEnum.LEVEL, */ MapObjectEnum.LAVA, MapObjectEnum.DOOR, MapObjectEnum.ROCK };
            this.currentrulesofthegame = new List<RuleObject>();
        }

        public void AddSaveGame(int currentMap)
        {
            throw new NotImplementedException();
        }

        public List<MapObject> LoadGame()
        {
            return null;
        }

        public void MoveEveryMovableObject(List<MapObject> cpm, int x, int y)
        {
            //Console.WriteLine("lefutottam " + this.width);
            //for (int j = 0; j < this.height / this.ObjectPixel; j++)
            //{
            //    for (int i = 0; i < this.width / this.ObjectPixel; i++)
            //    {
            //        Console.Write(cpm[i + j * this.width].MyProperty.ToString() + " " + cpm[i + j * this.width].Dx + " " + cpm[i + j * this.width].Dy + " | ");
            //    }
            //    Console.WriteLine("");
            //}

            //for (int i = 0; i < cpm.Count -1; i++) // -1 kell hogy a legutolsó elemet (ami fix fal) ne bántsa.
            //{
            //    if (cpm[i].MyProperty.Equals(MapObjectEnum.BABA))
            //    {
            //        if (y == 0)
            //        {
            //            //cpm[i].Dx += x * this.ObjectPixelWidth;
            //            //cpm[i + x].Dx -= x * this.ObjectPixelWidth;
            //            cpm[i].Move(x * this.ObjectPixelWidth, y * this.ObjectPixelHeight);
            //            cpm[i + x].Move(-1 * x * this.ObjectPixelWidth, y * this.ObjectPixelHeight);
            //            // Ki is cseréljük a két elemet! Zseniális!
            //            var temp = cpm[i];
            //            cpm[i] = cpm[i + x];
            //            cpm[i + x] = temp;
            //            i++; // Hogy az i+1 helyre tett BABA-t ne vizsgálja még egyszer! 
            //        }
            //        else if (x == 0)
            //        {
            //            //cpm[i].Dy += y * this.ObjectPixelHeight;
            //            //cpm[i + y].Dy -= y * this.ObjectPixelHeight;
            //            cpm[i].Move(x * this.ObjectPixelWidth, y * this.ObjectPixelHeight);
            //            cpm[i + (y * this.width)].Move(x * this.ObjectPixelWidth, -1 * y * this.ObjectPixelHeight);
            //            var temp = cpm[i];
            //            cpm[i] = cpm[i + (y * this.width)];
            //            cpm[i + (y * this.width)] = temp;
            //            i += this.width;
            //        }
            //    }
            //}

            for (int i = 0; i < cpm.Count -1; i++)
            {
                if (OperatorEnumList.Contains(cpm[i].MyProperty))
                {
                    if (i > 0 && i < (this.height - 1) * this.width - 2)
                    {
                        if (WORDEnumList.Contains(cpm[i - 1].MyProperty) && PropertiesEnumList.Contains(cpm[i + 1].MyProperty))
                        {
                            var temper = new RuleObject(cpm[i - 1].MyProperty, cpm[i].MyProperty, cpm[i + 1].MyProperty, VerticalHorizontalEnum.HORIZONTAL);
                            if (!ContainsWhether(temper))
                            {
                                Console.WriteLine("Szabály hozzáadva: " + temper.WORD + " " + temper.Operator + " " + temper.Property);
                                currentrulesofthegame.Add(temper);
                            }
                        }
                        else if (ForRemovalIndexOfRule(cpm[i]).VHE.Equals(VerticalHorizontalEnum.HORIZONTAL))
                        {
                            currentrulesofthegame.Remove(ForRemovalIndexOfRule(cpm[i]));
                        }
                    }
                    if (i > (this.width - 1) && i < (this.height - 1) * this.width)
                    {
                        if (WORDEnumList.Contains(cpm[i - this.width].MyProperty) && PropertiesEnumList.Contains(cpm[i + this.width].MyProperty))
                        {
                            var temper = new RuleObject(cpm[i - this.width].MyProperty, cpm[i].MyProperty, cpm[i + this.width].MyProperty, VerticalHorizontalEnum.VERTICAL);
                            if (!ContainsWhether(temper))
                            {
                                currentrulesofthegame.Add(temper);
                            }
                        }
                        else if (ForRemovalIndexOfRule(cpm[i]).VHE.Equals(VerticalHorizontalEnum.VERTICAL))
                        {
                            Console.WriteLine("NEM KÉNE");
                            currentrulesofthegame.Remove(ForRemovalIndexOfRule(cpm[i]));
                        }
                    }
                }
            }

            foreach (MapObject currentobject in cpm)
            {
                if (this.NotWordEnumList.Contains(currentobject.MyProperty))
                {
                    foreach (var rule in currentrulesofthegame)
                    {
                        if (this.WORDtoObject(rule.WORD).Equals(currentobject.MyProperty))
                        {
                            currentobject.RelevantBehaviors.Add(rule.Property);
                        }
                    }
                }
            }

            int indexofcpmnext = 0;
            int indexofcpmafternext = 0;
            List<int> theseareremovablebehaviorindexes = new List<int>();
            int howmanyofyoualiveyet = 0;
            foreach(var mek in cpm)
            {
                foreach(var tex in mek.RelevantBehaviors)
                {
                    if (tex.Equals(MapObjectEnum.YOU) || tex.Equals(MapObjectEnum.BABA)) howmanyofyoualiveyet++;
                }
            }
            for (int i = 0; i< cpm.Count; i++)
            {
                if (!cpm[i].MovedInThisTick)
                {
                    indexofcpmnext = 0;
                    indexofcpmafternext = 0;
                    if (x == 0 && i > 2 * (this.width - 1) && i < (this.height - 2) * this.width)
                    {
                        indexofcpmnext = i + (y * this.width);
                        indexofcpmafternext = i + (y * this.width) + (y * this.width);
                    }
                    else if (y == 0 && i > 1 && i < this.height * this.width - 2)
                    {
                        indexofcpmnext = i + x;
                        indexofcpmafternext = i + x + x;
                    }
                    if (cpm[i].RelevantBehaviors.Count > 0)
                    {
                        foreach (var rule in cpm[i].RelevantBehaviors)
                        {
                            if (rule.Equals(MapObjectEnum.YOU) || rule.Equals(MapObjectEnum.MOVE))
                            {
                                //var nexttothecurrentobject = ReturnMapObjectByIndex(cpm, cpm[i].Dx / this.ObjectPixelWidth + x, cpm[i].Dy / this.ObjectPixelHeight + y);
                                //var afterthenexttothecurrentobject = ReturnMapObjectByIndex(cpm, cpm[i].Dx / this.ObjectPixelWidth + x + x, cpm[i].Dy / this.ObjectPixelHeight + y + y);
                                //Console.WriteLine("VÉGE & cpm[i]: " + cpm[i].MyProperty + "|  Dx: " + cpm[i].Dx / this.ObjectPixel + "|  Dy: " + cpm[i].Dy / this.ObjectPixel + " | nexttothecurrentobject: " + nexttothecurrentobject.MyProperty + "|  Dx: " + nexttothecurrentobject.Dx / this.ObjectPixel + "| Dy: " + nexttothecurrentobject.Dy / this.ObjectPixel);
                                if (cpm[indexofcpmnext].MyProperty.Equals(MapObjectEnum.EMPTY))
                                {
                                    if (y == 0)
                                    {
                                        var imp = cpm[i].Dx;
                                        //cpm[i].Dx = cpm[indexofcpmnext].Dx;
                                        //cpm[indexofcpmnext].Dx = imp;
                                        cpm[i].Move(x * this.ObjectPixel, 0);
                                        cpm[indexofcpmnext].Move(-1 * x * this.ObjectPixel, 0);
                                    }
                                    else if (x == 0)
                                    {
                                        var imp = cpm[i].Dy;
                                        //cpm[i].Dy = cpm[indexofcpmnext].Dy;
                                        //cpm[indexofcpmnext].Dy = imp;
                                        cpm[i].Move(0, y * this.ObjectPixel);
                                        cpm[indexofcpmnext].Move(0, -1 * y * this.ObjectPixel);
                                    }
                                    var temp = cpm[indexofcpmnext];
                                    cpm[indexofcpmnext] = cpm[i];
                                    cpm[i] = temp;
                                    cpm[indexofcpmnext].MovedInThisTick = true;
                                }
                                else if (cpm[indexofcpmnext].MyProperty.Equals(MapObjectEnum.OPEN) && cpm[indexofcpmafternext].MyProperty.Equals(MapObjectEnum.EMPTY))
                                {
                                    if (y == 0)
                                    {
                                        var imp = cpm[i].Dx;
                                        //cpm[i].Dx = cpm[indexofcpmafternext].Dx;
                                        //cpm[indexofcpmafternext].Dx = cpm[i].Dx;
                                        cpm[i].Move(2 * x * this.ObjectPixel, 0);
                                        cpm[indexofcpmafternext].Move(-1 * 2 * x * this.ObjectPixel, 0);
                                    }
                                    else if (x == 0)
                                    {
                                        var imp = cpm[i].Dy;
                                        //cpm[i].Dy = cpm[indexofcpmafternext].Dy;
                                        //cpm[indexofcpmafternext].Dy = imp;
                                        cpm[i].Move(0, 2 * y * this.ObjectPixel);
                                        cpm[indexofcpmnext].Move(0, -1 * 2 * y * this.ObjectPixel);
                                    }
                                    var temp = cpm[indexofcpmafternext];
                                    cpm[indexofcpmafternext] = cpm[i];
                                    cpm[i] = temp;
                                    cpm[indexofcpmafternext].MovedInThisTick = true;
                                }
                                else
                                {
                                    foreach (var nexttorule in cpm[indexofcpmnext].RelevantBehaviors)
                                    {
                                        switch (nexttorule)
                                        {
                                            case MapObjectEnum.STOP:
                                                // semmi nem történik, hiszen nem mozdulhatsz ide! 
                                                // Nem kéne ideírni de így nem fut feleslegesen a switch case.
                                                break;
                                            case MapObjectEnum.SHUT:
                                                // semmi nem történik, hiszen nem mozdulhatsz ide! 
                                                // Nem kéne ideírni de így nem fut feleslegesen a switch case.
                                                break;
                                            case MapObjectEnum.WIN: this.VictoryOnThisMap(); break;
                                            case MapObjectEnum.PUSH:
                                                if (cpm[indexofcpmafternext].MyProperty.Equals(MapObjectEnum.EMPTY))
                                                {
                                                    if (y == 0)
                                                    {
                                                        // Csak egy kockát tud mozgatni, kettő pushable már nem jó!
                                                        //cpm[indexofcpmafternext].Dx = cpm[i].Dx;
                                                        //cpm[indexofcpmnext].Dx += x * this.ObjectPixel;
                                                        //cpm[i].Dx += x * this.ObjectPixel;
                                                        cpm[indexofcpmafternext].Move(- 2 * x * this.ObjectPixel,0);
                                                        cpm[indexofcpmnext].Move(x * this.ObjectPixel,0);
                                                        cpm[i].Move(x * this.ObjectPixel, 0);
                                                    }
                                                    else if (x == 0)
                                                    {
                                                        //cpm[indexofcpmafternext].Dx = cpm[i].Dy;
                                                        //cpm[indexofcpmnext].Dy += y * this.ObjectPixel;
                                                        //cpm[i].Dy += y * this.ObjectPixel;
                                                        cpm[indexofcpmafternext].Move(0, -2 * y * this.ObjectPixel);
                                                        cpm[indexofcpmnext].Move(0, y * this.ObjectPixel);
                                                        cpm[i].Move(0, y * this.ObjectPixel);
                                                    }
                                                    //var temp = cpm[indexofcpmnext];
                                                    var tempnext = cpm[indexofcpmafternext];
                                                    cpm[indexofcpmafternext] = cpm[indexofcpmnext];
                                                    cpm[indexofcpmnext] = cpm[i];
                                                    cpm[i] = tempnext;
                                                    cpm[indexofcpmnext].MovedInThisTick = true;
                                                    cpm[indexofcpmafternext].MovedInThisTick = true;
                                                }
                                                break;
                                            case MapObjectEnum.SINK:
                                                cpm[i].MyProperty = MapObjectEnum.EMPTY;
                                                theseareremovablebehaviorindexes.Add(i);
                                                //cpm[i].RelevantBehaviors.Clear();
                                                cpm[indexofcpmnext].MyProperty = MapObjectEnum.EMPTY;
                                                theseareremovablebehaviorindexes.Add(indexofcpmnext);
                                                //cpm[indexofcpmnext].RelevantBehaviors.Clear();
                                                break;
                                            case MapObjectEnum.DEFEAT:
                                                cpm[i].MyProperty = MapObjectEnum.EMPTY;
                                                theseareremovablebehaviorindexes.Add(i);
                                                //cpm[i].RelevantBehaviors.Clear();
                                                howmanyofyoualiveyet--;
                                                //foreach (var rules in cpm)
                                                //{
                                                //    if (rules.MyProperty.Equals(MapObjectEnum.YOU))
                                                //    {
                                                //        howmanyofyoualiveyet++;
                                                //    }
                                                //}
                                                //if (howmanyofyou == 0)
                                                //{
                                                //    this.DefeatOnThisMap();
                                                //}
                                                if (howmanyofyoualiveyet < 1) this.DefeatOnThisMap();
                                                break;
                                            case MapObjectEnum.SWAP:
                                                if (y == 0)
                                                {
                                                    //cpm[i].Dx += x * this.ObjectPixel;
                                                    //cpm[indexofcpmnext].Move(x * this.ObjectPixel,0);
                                                    cpm[i].Move(x * this.ObjectPixel,0);
                                                    cpm[indexofcpmnext].Move(-1 * x * this.ObjectPixel, 0);
                                                }
                                                else if (x == 0)
                                                {
                                                    //cpm[i].Dy += y * this.ObjectPixel;
                                                    //cpm[indexofcpmnext].Dy -= y * this.ObjectPixel;
                                                    cpm[i].Move(0, y * this.ObjectPixel);
                                                    cpm[indexofcpmnext].Move(0, -1 * y * this.ObjectPixel);
                                                }
                                                var temper = cpm[indexofcpmnext];
                                                cpm[indexofcpmnext] = cpm[i];
                                                cpm[i] = temper;
                                                cpm[i].MovedInThisTick = true;
                                                cpm[indexofcpmnext].MovedInThisTick = true;
                                                break;
                                        }
                                        if (cpm[indexofcpmnext].RelevantBehaviors.Count < 1) break;
                                    }
                                }
                                // Console.WriteLine("VÉGE & cpm[i]: " + cpm[i].MyProperty + "|  Dx: " + cpm[i].Dx / this.ObjectPixel + "|  Dy: " + cpm[i].Dy / this.ObjectPixel + " | nexttothecurrentobject: " + nexttothecurrentobject.MyProperty + "|  Dx: " + nexttothecurrentobject.Dx / this.ObjectPixel + "| Dy: " + nexttothecurrentobject.Dy / this.ObjectPixel);
                            }
                        }
                    }
                    foreach (var xe in theseareremovablebehaviorindexes)
                    {
                        cpm[xe].RelevantBehaviors.Clear();
                    }
                }
            }

            //MoveEveryMovableObject(List < MapObject > cpm, int x, int y)
            foreach (var xe in cpm)
            {
                xe.MovedInThisTick = false;
            }

            // szépen vissza kell állítani hogy kövi tick alatt mozoghassanak
            foreach (var xe in cpm)
            {
                xe.MovedInThisTick = false;
            }
            //for (int i = 0; i < this.height; i++)
            //{
            //    for (int j = 0; j < this.width; j++)
            //    {
            //        Console.Write(cpm[i * this.width + j].MyProperty.ToString() + " : " + cpm[i * this.width + j].Dx + " : " + cpm[i * this.width + j].Dy + " @_@ ");
            //    }
            //    Console.WriteLine("");
            //}
            //Console.WriteLine("");
            //Console.WriteLine("");
            //Console.WriteLine("");
        }

        private MapObjectEnum WORDtoObject(MapObjectEnum wORD)
        {
            switch (wORD)
            {
                case MapObjectEnum.BABA_WORD: return MapObjectEnum.BABA;
                case MapObjectEnum.EMPTY_WORD: return MapObjectEnum.EMPTY;
                case MapObjectEnum.WALL_WORD: return MapObjectEnum.WALL;
                case MapObjectEnum.WATER_WORD: return MapObjectEnum.WATER;
                case MapObjectEnum.SKULL_WORD: return MapObjectEnum.SKULL;
                case MapObjectEnum.FENCE_WORD: return MapObjectEnum.FENCE;
                case MapObjectEnum.HEDGE_WORD: return MapObjectEnum.HEDGE;
                case MapObjectEnum.TREE_WORD: return MapObjectEnum.TREE;
                case MapObjectEnum.FLAG_WORD: return MapObjectEnum.FLAG;
                case MapObjectEnum.KEY_WORD: return MapObjectEnum.KEY;
                case MapObjectEnum.GRASS_WORD: return MapObjectEnum.GRASS;
                case MapObjectEnum.KEKE_WORD: return MapObjectEnum.KEKE;
                case MapObjectEnum.ICE_WORD: return MapObjectEnum.ICE;
                case MapObjectEnum.BELT_WORD: return MapObjectEnum.BELT;
                case MapObjectEnum.BOX_WORD: return MapObjectEnum.BOX;
                case MapObjectEnum.LEVEL_WORD: return MapObjectEnum.LEVEL_WORD;
                case MapObjectEnum.LAVA_WORD: return MapObjectEnum.LAVA;
                case MapObjectEnum.DOOR_WORD: return MapObjectEnum.DOOR;
                case MapObjectEnum.ROCK_WORD: return MapObjectEnum.ROCK;
            }
            return wORD;
        }

        private void DefeatOnThisMap()
        {
            Console.WriteLine("defeat!");
            // Irány vissza a menübe! Nem mentünk vereséget, hisza  map megcnjyitása előtti állapot az érvényes;
        }

        private void VictoryOnThisMap()
        {
            // Majd visszadob a pályaválasztó menübe, miközben elment minden releváns infót;
        }

        public string[,] NewTickSoFreshMapMaking(List<MapObject> currentlyPlayedMap)
        {
            var kimenet = new string[this.height, this.width];
            foreach (var viewedmapobject in currentlyPlayedMap)
            {
                kimenet[viewedmapobject.Dy / ObjectPixel, viewedmapobject.Dx / ObjectPixel] = Enum.GetName(typeof(MapObjectEnum), viewedmapobject.MyProperty);
            }
            return kimenet;
        }

        public List<string> ScanAllFramesOfGif(string temp)
        {
            return repository.ScanAllFramesOfGif(temp);
        }

        public List<List<MapObject>> ScanAllMaps()
        {
            return repository.ScanAllMaps();
        }

        public List<string> ScanAllPictureNames()
        {
            return repository.ScanAllPictureNames();
        }

        private bool ContainsWhether(RuleObject input)
        {
            foreach(var x in currentrulesofthegame)
            {
                if(x.Operator.Equals(input.Operator) && x.Property.Equals(input.Property) && x.WORD.Equals(input.WORD))
                {
                    return true;
                }
            }
            return false;
        }
        private RuleObject ForRemovalIndexOfRule(MapObject input)
        {
            foreach (var x in currentrulesofthegame)
            {
                if (x.Operator.Equals(input.MyProperty))
                {
                    return x;
                }
            }
            return null;
        }

        private MapObject ReturnMapObjectByIndex(List<MapObject> cpm, int x, int y)
        {
            return cpm[y * (this.width) + x];
        }
    }
}
