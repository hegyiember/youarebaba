﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using YouAreBaba.Model;

namespace YouAreBaba.Repositorys
{
    public class DatabaseRepository : IDatabaseRepository
    {
        // HIGHSCORE ******************************************************

        private XDocument allhighscore;
        private int maxnum;

        public DatabaseRepository()
        {
            allhighscore = XDocument.Load(@"../../../YouAreBaba.Repository/Highscore/Highscores.xml");
            this.maxnum = 13;
        }
        public List<Highscore> Get()
        {
            var temper = new List<Highscore>();

            var dabber = allhighscore.Element("Highscore").Elements("User").ToList();

            foreach (var x in dabber)
            {
                var tew = Convert.ToInt32(x.Element("MapID").Value);
                /*if(tew < this.maxnum)*/ temper.Add(new Highscore()
                {
                    Name = x.Element("Name").Value,
                    Mapid = tew,
                    Time = Convert.ToInt32(x.Element("Time").Value)
                });
            }
            return temper;
        }
        public bool Add(string name, int mapid, int time)
        {
            var wastherelikethis = allhighscore.Element("Highscore").Elements("User").Where(x => x.Element("Name").Value.Equals(name)).Where(y => Convert.ToInt32(y.Element("MapID").Value) == mapid);
            if(wastherelikethis.Count() == 0)
            {
                XElement emp = new XElement("User",
                new XElement("Name", name),
                new XElement("MapID", mapid),
                new XElement("Time", time));
                //if(Convert.ToInt32(emp.Element("MapID").Value) 
                //    < allhighscore.Element("Highscore").Elements("User").Max(x => Convert.ToInt32(x.Element("MapID").Value)))
                //{
                    allhighscore.Element("Highscore").Add(emp);
                    allhighscore.Save(@"../../../YouAreBaba.Repository/Highscore/Highscores.xml");
                //}
                return true;
            }
            else if(wastherelikethis.Count() > 0)
            {
                wastherelikethis.First().Element("MapID").SetValue(time);
                return true;
            }
            else
            {
                return false;
            }
        }


        public bool DeleteAllRecord(string name)
        {
            allhighscore.Element("Highscore").Elements("User").Where(x => x.Element("Name").Value.Equals(name)).Remove();
            allhighscore.Save(@"../../../YouAreBaba.Repository/Highscore/Highscores.xml");
            return true;
        }
        // HIGHSCORE ******************************************************
    }
}
