﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouAreBaba.Model;

namespace YouAreBaba.Repositorys
{
    public interface IUserRepository
    {

        // HIGHSCORE ******************************************************
        List<Highscore> Get();

        bool Add(string name, int mapid, int time);

        bool DeleteAllRecord(string name);
        // HIGHSCORE ******************************************************
    }
}
