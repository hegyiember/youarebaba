﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouAreBaba.Data;
using YouAreBaba.Model;

namespace YouAreBaba.Repositorys
{
    public class UserRepositoy : IUserRepository
    {
        private DBEntities dB { get; set; }

        public UserRepositoy()
        {
            this.dB = new DBEntities();
        }
        // HIGHSCORE ******************************************************

        public List<Highscore> Get()
        {
            var temper = new List<Highscore>();
            foreach (var x in this.dB.HIGHSCOREs.ToList())
            {
                temper.Add(new Highscore()
                {
                    Id = (int)x.ID,
                    Name = x.NAME,
                    Mapid = (int)x.MAPID,
                    Time = (int)x.TIME
                });
            }
            return temper;
        }
        public bool Add(string name, int mapid, int time)
        {
            var distinct = from x in this.dB.HIGHSCOREs where x.NAME.Equals(name) && x.MAPID == mapid select x.ID;
            if (distinct.Count() == 0)
            {
                var ide = this.dB.HIGHSCOREs.Count() + 1;
                this.dB.HIGHSCOREs.Add(new HIGHSCORE
                {
                    ID = ide,
                    NAME = name,
                    MAPID = mapid,
                    TIME = time
                });
                this.dB.SaveChanges();
                return true;
            }
            else if (distinct.Count() == 1)
            {
                var refer = this.dB.HIGHSCOREs.SingleOrDefault(b => b.NAME.Equals(name));
                if (refer != null)
                {
                    refer.TIME = time;
                    this.dB.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public bool DeleteAllRecord(string name)
        {
            var templist = (from x in this.dB.HIGHSCOREs where x.NAME.Equals(name) select x).ToList();
            if (templist.Count < 1)
            {
                return false;
            }
            foreach (var x in templist)
            {
                this.dB.HIGHSCOREs.Remove(x);
            }
            return true;
        }
        // HIGHSCORE ******************************************************
    }
}
