﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouAreBaba.Model;

namespace YouAreBaba.Repositorys
{
    public interface IRepository
    {
        List<string> ScanAllPictureNames();
        List<List<MapObject>> ScanAllMaps();
        List<string> ScanAllFramesOfGif(string temp);
        void AddSaveGame(int currentMap, List<MapObject> currentlyPlayedMap);
        List<int[]> ScanMapSizes();
        List<MapObject> LoadGame();
    }
}
