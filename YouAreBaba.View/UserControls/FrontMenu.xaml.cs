﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfAnimatedGif;
using YouAreBaba.ViewModel;

namespace YouAreBaba.View.UserControls
{
    /// <summary>
    /// Interaction logic for FrontMenu.xaml
    /// </summary>
    public partial class FrontMenu : UserControl
    {
        public FrontMenu()
        {
            InitializeComponent();
            (Application.Current.MainWindow.DataContext as MenuViewModel).PositionHeight = 1080 / 2 - 300;
            (Application.Current.MainWindow.DataContext as MenuViewModel).PositionWidth = 1920 / 2 - 400;
            (Application.Current.MainWindow.DataContext as MenuViewModel).ChangeVictoriousPlayedMapButtonToGreen();
        }

        private void Choose_Map_Button_Click(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("FrontMenu Choose_Map_Button_Click SwitchView");
            (Window.GetWindow(this).DataContext as MenuViewModel).SwitchView = 2;
        }

        private void Exit_Button_Click(object sender, RoutedEventArgs e)
        {
            Window.GetWindow(this).Close();
        }

        private void Rules_Button_Click(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("FrontMenu Rules_Button_Click SwitchView");
            (Window.GetWindow(this).DataContext as MenuViewModel).SwitchView = 3;
        }

        private void Load_Button_Click(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("FrontMenu Load_Button_Click SwitchView");
            (Window.GetWindow(this).DataContext as MenuViewModel).SwitchView = 1;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            (Window.GetWindow(this).DataContext as MenuViewModel).CleanLastPlayedMap((Window.GetWindow(this).DataContext as MenuViewModel).GamerName);
            (Window.GetWindow(this).DataContext as MenuViewModel).ChangeAllPlayedMapButtonToNothing();
            //(Window.GetWindow(this).DataContext as MenuViewModel).Clean
            MessageBox.Show("Highscore deleted");
        }
        private void Highscore_Click(object sender, RoutedEventArgs e)
        {
            (Window.GetWindow(this).DataContext as MenuViewModel).SwitchView = 4;
        }

        private void Change_Click(object sender, RoutedEventArgs e)
        {
            (Window.GetWindow(this).DataContext as MenuViewModel).SwitchView = 5;
        }
    }
}
