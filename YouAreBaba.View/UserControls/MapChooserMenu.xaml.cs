﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfAnimatedGif;
using YouAreBaba.ViewModel;

namespace YouAreBaba.View.UserControls
{
    /// <summary>
    /// Interaction logic for MapChooserMenu.xaml
    /// </summary>
    public partial class MapChooserMenu : UserControl
    {
        public MapChooserMenu()
        {
            InitializeComponent();
            (Application.Current.MainWindow.DataContext as MenuViewModel).PositionHeight = 1080 / 2 - 300;
            (Application.Current.MainWindow.DataContext as MenuViewModel).PositionWidth = 1920 / 2 - 450;
        }


        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var mapid = (sender as Button).Content.ToString();
            if ((Application.Current.MainWindow.DataContext as MenuViewModel).CanThisMapBePlayedByUser(Convert.ToInt32(mapid)))
            {
                App.Current.Resources["CurrentMap"] = mapid;
                Console.WriteLine("MapChooserMenu SwitchView");
                (Window.GetWindow(this).DataContext as MenuViewModel).SwitchView = 1;
            }
        }
    }
}
