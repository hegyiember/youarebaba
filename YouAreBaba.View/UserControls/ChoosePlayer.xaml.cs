﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfAnimatedGif;
using YouAreBaba.ViewModel;

namespace YouAreBaba.View.UserControls
{
    /// <summary>
    /// Interaction logic for ChoosePlayer.xaml
    /// </summary>
    public partial class ChoosePlayer : UserControl
    {
        public ChoosePlayer()
        {
            InitializeComponent();
            (Application.Current.MainWindow.DataContext as MenuViewModel).PositionHeight = 1080 / 2 - 300;
            (Application.Current.MainWindow.DataContext as MenuViewModel).PositionWidth = 1920 / 2 - 400;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var te = (this.DataContext as UserViewModel).Usernames.Contains((this.DataContext as UserViewModel).ChosenUser);
            if ((this.DataContext as UserViewModel).BoolchosenUser && te)
            {
                (Application.Current.MainWindow.DataContext as MenuViewModel).GamerName = (this.DataContext as UserViewModel).ChosenUser;
                var mapidk = (Application.Current.MainWindow.DataContext as MenuViewModel).GetAllVictoryMapNumberByUser(
                       (Application.Current.MainWindow.DataContext as MenuViewModel).GamerName
                       );
                if (mapidk.Count == 0)
                {
                    (Application.Current.MainWindow.DataContext as MenuViewModel).LastPlayedMap = 0;
                }
                else
                {

                    (Application.Current.MainWindow.DataContext as MenuViewModel).LastPlayedMap = mapidk.Last();
                }
                (Window.GetWindow(this).DataContext as MenuViewModel).SwitchView = 0;
            }
            else if ((this.DataContext as UserViewModel).BoolchosenUser)
            {
                (this.DataContext as UserViewModel).Add((this.DataContext as UserViewModel).ChosenUser, 0, 0);
                (Application.Current.MainWindow.DataContext as MenuViewModel).GamerName = (this.DataContext as UserViewModel).ChosenUser;
                var mapidk = (Application.Current.MainWindow.DataContext as MenuViewModel).GetAllVictoryMapNumberByUser(
                       (Application.Current.MainWindow.DataContext as MenuViewModel).GamerName
                       );
                if (mapidk.Count == 0)
                {
                    (Application.Current.MainWindow.DataContext as MenuViewModel).LastPlayedMap = 0;
                }
                else
                {
                    (Application.Current.MainWindow.DataContext as MenuViewModel).LastPlayedMap = mapidk.Last();
                }
                (Window.GetWindow(this).DataContext as MenuViewModel).SwitchView = 0;
            }
            else if (!te && (this.DataContext as UserViewModel).FromListChosenUserBool)
            {
                (Application.Current.MainWindow.DataContext as MenuViewModel).GamerName = (this.DataContext as UserViewModel).FromListChosenUser;
                var mapidk = (Application.Current.MainWindow.DataContext as MenuViewModel).GetAllVictoryMapNumberByUser(
                       (Application.Current.MainWindow.DataContext as MenuViewModel).GamerName
                       );
                if (mapidk.Count == 0)
                {
                    (Application.Current.MainWindow.DataContext as MenuViewModel).LastPlayedMap = 0;
                }
                else
                {
                    (Application.Current.MainWindow.DataContext as MenuViewModel).LastPlayedMap = mapidk.Last();
                }
                (Window.GetWindow(this).DataContext as MenuViewModel).SwitchView = 0;
            }
            else if ((Application.Current.MainWindow.DataContext as MenuViewModel).GamerName == null)
            {
                Application.Current.MainWindow.Close();
            }
        }

        private void ListBox_Selected(object sender, RoutedEventArgs e)
        {
            (this.DataContext as UserViewModel).FromListChosenUserBool = true
                ;
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            (this.DataContext as UserViewModel).BoolchosenUser = true;
        }
    }
}
