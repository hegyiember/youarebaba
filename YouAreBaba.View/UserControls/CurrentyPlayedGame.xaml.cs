﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using YouAreBaba.ViewModel;

namespace YouAreBaba.View.UserControls
{
    /// <summary>
    /// Interaction logic for CurrentyPlayedGame.xaml
    /// </summary>
    public partial class CurrentyPlayedGame : UserControl
    {
        public CurrentyPlayedGame()
        {
            if (!(Application.Current.MainWindow.DataContext as MenuViewModel).GameViewhasbeenloaded)
            {
                (Application.Current.MainWindow.DataContext as MenuViewModel).GameViewhasbeenloaded = true;
                InitializeComponent();
                this.MainGrid.Content = new GameScreen();
                (Application.Current.MainWindow.DataContext as MenuViewModel).SetMapForGameplay(
                    (this.MainGrid.Content as GameScreen).ActiveMapID
                    , (this.MainGrid.Content as GameScreen).ObjectPixel
                    );
                this.InvalidateVisual();
                Console.WriteLine("CurrentyPlayedGame");
            }
        }
    }
}
