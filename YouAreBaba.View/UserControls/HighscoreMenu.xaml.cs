﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfAnimatedGif;
using YouAreBaba.ViewModel;

namespace YouAreBaba.View.UserControls
{
    /// <summary>
    /// Interaction logic for HighscoreMenu.xaml
    /// </summary>
    public partial class HighscoreMenu : UserControl
    {
        public HighscoreMenu()
        {
            InitializeComponent();
            (Application.Current.MainWindow.DataContext as MenuViewModel).PositionHeight = 1080 / 2 - 300;
            (Application.Current.MainWindow.DataContext as MenuViewModel).PositionWidth = 1920 / 2 - 400;
            Liste.ItemsSource = (Application.Current.MainWindow.DataContext as MenuViewModel).Get().OrderByDescending(x => x.Mapid).ThenByDescending(y => y.Time);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            (Window.GetWindow(this).DataContext as MenuViewModel).SwitchView = 0;
        }
    }
}
