﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using YouAreBaba.ViewModel;

namespace YouAreBaba.View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MusicFactory musicFactory;
        public MusicFactory MusicFactory { get; }
        public MainWindow()
        {
            InitializeComponent();
            Console.WriteLine("MainWindow");
            musicFactory = new MusicFactory();
            musicFactory.PlayABackgroudnMusic();
        }



        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if((Application.Current.MainWindow.DataContext as MenuViewModel).SwitchView == 2 && e.Key.Equals(Key.Escape)) (Application.Current.MainWindow.DataContext as MenuViewModel).SwitchView = 0;
        }
    }
}
