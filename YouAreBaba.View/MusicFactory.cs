﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouAreBaba.View
{
    public class MusicFactory
    {
        private List<string> music;
        private List<string> sfx;
        private System.Media.SoundPlayer player;
        private Random rnd;
        public MusicFactory()
        {
            this.player = new System.Media.SoundPlayer();
            this.music = new List<string>();
            this.sfx = new List<string>();
            this.rnd = new Random();
            this.ReadAllMusic();
        }
        private void ReadAllMusic()
        {
            var directory = new DirectoryInfo(@"../../Music/background");
            foreach (var x in directory.GetFiles())
            {
                music.Add(x.FullName);
            }
        }
        public void PlayABackgroudnMusic()
        {
            player.SoundLocation = music[rnd.Next(0, music.Count - 1)];
            player.PlayLooping();
        }
    }
}
