﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouAreBaba.Logic
{
    public interface IMenuLogic
    {
        void NewGame();
        void ChooseMap();
        void LoadLastPlayedMap();
        void Highscores();
        void Rulesandcontrols();
    }
}
