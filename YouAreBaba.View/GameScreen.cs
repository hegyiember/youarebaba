﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using WpfAnimatedGif;
using YouAreBaba.Model;
using YouAreBaba.ViewModel;

namespace YouAreBaba.View
{
    public class PictureStruct
    {
        public PictureStruct(string GifPictureName, int MaximumSecondPart)
        {
            this.ObjectImageFrames = new List<BitmapImage>();
            this.GifPictureName = GifPictureName;
            this.currentsecondpart = 0;
            this.MaximumSecondPart = MaximumSecondPart - 1;
        }
        public List<BitmapImage> ObjectImageFrames { get; set; }
        public string GifPictureName { get; set; }
        public BitmapImage CurrentObjectImage { get { return ObjectImageFrames[CurrentSecondPart]; } }
        private int currentsecondpart;
        public int CurrentSecondPart { get { return currentsecondpart; } }
        public int MaximumSecondPart { get; set; }
        public void IncreaseCurrentSecondPart()
        {
            if (currentsecondpart < MaximumSecondPart) currentsecondpart++;
            else currentsecondpart = 0;
        }
    }
    public class GameScreen : FrameworkElement
    {
        private IGameViewModel gameModel;
        private DispatcherTimer timer;
        private DispatcherTimer timer2;
        private DispatcherTimer timer3;
        private Stopwatch timerall;
        private int GameWidth { get; set; }
        private int GameHeight { get; set; }
        public int ObjectPixel { get; set; }
        private int IamSpeed { get; set; }
        private int IsKeyDown { get; set; }

        private List<PictureStruct> ObjectImages { get; set; }
        private List<string> AllPictureNames { get; set; }
        private int NumberOfUniformFrames { get; set; }
        private int FPSSpeed { get; set; }
        private List<int[]> MapInformations { get; set; }
        public int ActiveMapID { get; set; }
        private int temp;

        private int smootmovingtime;

        private int x;
        private int y;
        private bool hassmoothmovingended;

        public GameScreen()
        {
            (Application.Current.MainWindow.DataContext as MenuViewModel).MenuOrGame = false;
            this.temp = 0;
            this.IsKeyDown = 0;
            // Ez itt felesleges, csak elrontja a map betöltést, de placeholdernek itt marad még //App.Current.Resources["CurrentMap"] = (Application.Current.MainWindow.DataContext as MenuViewModel).FromSavedGameFinishedMap.Max();
            this.ActiveMapID = Convert.ToInt32(App.Current.Resources["CurrentMap"]);
            this.MapInformations = (Application.Current.MainWindow.DataContext as MenuViewModel).ScanMapSizes();


            this.ObjectPixel = 36;
            this.IamSpeed = 1;
            this.GameHeight = MapInformations[ActiveMapID][2];
            this.GameWidth = MapInformations[ActiveMapID][1];
            this.NumberOfUniformFrames = 3;
            this.FPSSpeed = 200;

            this.gameModel = new GameViewModel(this.GameWidth, this.GameHeight, this.ObjectPixel, this.ActiveMapID);
            this.Loaded += this.GameScreenLoaded;
            this.timer = new DispatcherTimer();
            this.timer2 = new DispatcherTimer();
            this.timer3 = new DispatcherTimer();
            this.timerall = new Stopwatch();
            this.timerall.Start();
            this.AllPictureNames = gameModel.ScanAllPictureNames();
            this.ObjectImages = new List<PictureStruct>();
            this.ConnectAllImages();
            (Application.Current.MainWindow.DataContext as MenuViewModel).SetGAmeBackgroundColor(this.ActiveMapID);
            Application.Current.MainWindow.Height = (double)this.GameHeight * this.ObjectPixel + 38;
            Application.Current.MainWindow.Width = (double)this.GameWidth * this.ObjectPixel + 14;

            this.smootmovingtime = 0;
            this.x = 0;
            this.y = 0;
            this.hassmoothmovingended = true;

            (Application.Current.MainWindow.DataContext as MenuViewModel).CanLoad = false;
        }

        /// <summary>
        /// Rendering graphical elements
        /// </summary>
        /// <param name="drawingContext">The drawing instructions</param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            List<MapObject> temp;
            //drawingContext.DrawRectangle(new SolidColorBrush(Color.FromArgb(255, (byte)57, (byte)69, (byte)66)), null, new System.Windows.Rect(
            //    2 * this.ObjectPixel,
            //    2 * this.ObjectPixel,
            //    (this.GameWidth - 4) * this.ObjectPixel, 
            //    (this.GameHeight - 4) * this.ObjectPixel
            //    ));
            if (this.smootmovingtime == 1 ) temp = gameModel.PrevoiusmoveCurrentlyPlayedMap;
            else temp = gameModel.CurrentlyPlayedMap;
            foreach (var inc in temp)
            {
                var giftemp = this.ObjectImages.Where(s => s.GifPictureName.Equals(inc.MyProperty.ToString())).Select(s => s.CurrentObjectImage).First();
                System.Windows.Rect kek = new System.Windows.Rect(inc.Area.X, inc.Area.Y, inc.Area.Width, inc.Area.Height);
                if (!inc.MyProperty.Equals(MapObjectEnum.EMPTY) && !inc.MyProperty.Equals(MapObjectEnum.KERET))
                {
                    drawingContext.DrawRectangle(new ImageBrush(giftemp), null, kek);
                }

            }
        }

        private void GameLogic_Death(object sender, EventArgs e)
        {
            (this.DataContext as MenuViewModel).GameViewhasbeenloaded = false;
            Window.GetWindow(this).Close();
        }

        private void GameScreenLoaded(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("GameScreenLoaded");
            Application.Current.MainWindow.KeyDown += this.Window_KeyDown;
            Application.Current.MainWindow.KeyUp += this.Window_KeyUp;

            this.timer.Interval = TimeSpan.FromMilliseconds(this.IamSpeed); // 16
            this.timer.Tick += this.Timer_Tick;
            this.timer.Start();
            this.timer2.Interval = TimeSpan.FromMilliseconds(this.FPSSpeed); // FPSSpeed
            this.timer2.Tick += this.Timer2_Tick;
            this.timer2.Start();
            this.timer3.Interval = TimeSpan.FromMilliseconds(this.IamSpeed); // FPSSpeed
            this.timer3.Tick += this.Timer3_Tick;
            this.timer3.Start();
        }

        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            this.IsKeyDown = 0;
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if ((this.x != 0 || this.y != 0) && this.smootmovingtime < this.ObjectPixel / 2)
            {
                this.hassmoothmovingended = false;
                this.smootmovingtime++;
                if(this.smootmovingtime != 0) this.gameModel.MoveSmoothlyPixels(x, y, smootmovingtime);
            }
            else if(this.smootmovingtime >= this.ObjectPixel / 2)
            {
                this.smootmovingtime = 0;
                this.x = 0;
                this.y = 0;
                this.hassmoothmovingended = true;
                foreach (var xd in this.gameModel.CurrentlyPlayedMap) xd.RelevantBehaviors.Clear();
            }
        }
        private void Timer2_Tick(object sender, EventArgs e)
        {
            foreach(var x in ObjectImages) x.IncreaseCurrentSecondPart();
        }
        private void Timer3_Tick(object sender, EventArgs e)
        {
            this.InvalidateVisual();
        }

        private void Window_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (IsKeyDown == 0)
            {
                Console.WriteLine("Window_KeyDown");
                switch (e.Key)
                {
                    case Key.Left: temp = this.SmoothMoving(-1, 0); break;
                    case Key.Right: temp = this.SmoothMoving(1, 0); break;
                    case Key.Up: temp = this.SmoothMoving(0, -1); break;
                    case Key.Down: temp = this.SmoothMoving(0, 1); break;
                    case Key.F5:
                        this.timer.Stop(); this.timer2.Stop(); this.timer3.Stop(); this.timerall.Stop();
                        this.gameModel.AddSaveGame();
                        MessageBox.Show("Game saved!");
                        (Application.Current.MainWindow.DataContext as MenuViewModel).CanLoad = true;
                        this.timer.Start(); this.timer2.Start(); this.timer3.Start(); this.timerall.Start();
                        break;
                    case Key.F6:
                        if ((Application.Current.MainWindow.DataContext as MenuViewModel).CanLoad)
                        {
                            this.timer.Stop(); this.timer2.Stop(); this.timer3.Stop(); this.timerall.Stop();
                            this.gameModel.LoadGame();
                            MessageBox.Show("Game loaded!");
                            this.timer.Start(); this.timer2.Start(); this.timer3.Start(); this.timerall.Start(); 
                        }
                        break;
                    case Key.Escape:
                        Application.Current.MainWindow.KeyDown -= this.Window_KeyDown;
                        Application.Current.MainWindow.KeyUp -= this.Window_KeyUp;
                        this.timer.Stop();
                        this.timer2.Stop();
                        this.timerall.Stop();
                        MessageBox.Show("Exit!");
                        (this.DataContext as MenuViewModel).GameViewhasbeenloaded = false;
                        (Application.Current.MainWindow.DataContext as MenuViewModel).LastPlayedMap = gameModel.CurrentMap;
                        App.Current.Resources["CurrentMap"] = gameModel.CurrentMap;
                        this.gameModel = null;
                        (Application.Current.MainWindow.DataContext as MenuViewModel).MenuOrGame = true;
                        (Application.Current.MainWindow.DataContext as MenuViewModel).SwitchView = 2;
                        break;
                }
                if (temp == 2)
                {
                    Application.Current.MainWindow.KeyDown -= this.Window_KeyDown;
                    Application.Current.MainWindow.KeyUp -= this.Window_KeyUp;
                    this.timer.Stop();
                    this.timer2.Stop();
                    this.timerall.Stop();
                    var elapsedtime = (int)this.timerall.Elapsed.TotalSeconds;
                    (Application.Current.MainWindow.DataContext as MenuViewModel).Add(
                        (Application.Current.MainWindow.DataContext as MenuViewModel).GamerName,
                        this.ActiveMapID, elapsedtime
                    );
                    this.temp = 0;
                    MessageBox.Show("Winning!");
                    (this.DataContext as MenuViewModel).GameViewhasbeenloaded = false;
                    (Application.Current.MainWindow.DataContext as MenuViewModel).LastPlayedMap = gameModel.CurrentMap;
                    App.Current.Resources["CurrentMap"] = gameModel.CurrentMap;
                    (Application.Current.MainWindow.DataContext as MenuViewModel).ChangeLastPlayedMapButtonToGreen();
                    this.gameModel = null;
                    (Application.Current.MainWindow.DataContext as MenuViewModel).MenuOrGame = true;
                    (Application.Current.MainWindow.DataContext as MenuViewModel).SwitchView = 2;
                }
                else if (temp == 1)
                {
                    Application.Current.MainWindow.KeyDown -= this.Window_KeyDown;
                    Application.Current.MainWindow.KeyUp -= this.Window_KeyUp;
                    this.timer.Stop();
                    this.timer2.Stop();
                    this.timerall.Stop();
                    this.temp = 0;
                    MessageBox.Show("Losing!");
                    (this.DataContext as MenuViewModel).GameViewhasbeenloaded = false;
                    (Application.Current.MainWindow.DataContext as MenuViewModel).LastPlayedMap = gameModel.CurrentMap;
                    App.Current.Resources["CurrentMap"] = gameModel.CurrentMap;
                    (Application.Current.MainWindow.DataContext as MenuViewModel).ChangeLastPlayedMapButtonToRed();
                    this.gameModel = null;
                    (Application.Current.MainWindow.DataContext as MenuViewModel).MenuOrGame = true;
                    (Application.Current.MainWindow.DataContext as MenuViewModel).SwitchView = 2;
                }
            }
            this.IsKeyDown++;
            
        }

        private int SmoothMoving(int x, int y)
        {
            if (this.hassmoothmovingended)
            {
                int kim = this.gameModel.Move(x, y);
                this.x = x;
                this.y = y;
                return kim; 
            }
            else
            {
                return 0;
            }
        }
        private void ConnectAllImages()
        {
            foreach(var temp in AllPictureNames)
            {
                List<string> framesofthisgif = this.gameModel.ScanAllFramesOfGif(temp);
                ObjectImages.Add(new PictureStruct(temp, NumberOfUniformFrames));
                foreach (var x in framesofthisgif) ObjectImages.Last().ObjectImageFrames.Add(new BitmapImage(new Uri(@"../../Pictures/" + temp + "/" + x + ".png", UriKind.Relative)));
            }
        }
    }
}
