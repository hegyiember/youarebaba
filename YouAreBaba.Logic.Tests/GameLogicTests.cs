using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using YouAreBaba.Model;


namespace YouAreBaba.Logic.Tests
{
    [TestFixture]
    class GameLogicTests
    {
        [Test]
        public void ThereAreNoRulesOnAnEmptyMap_Test()
        {
            List<MapObject> tesztPalya = new List<MapObject>();
            for (int i = 0; i < 12; i++) //sor
            {
                for (int j = 0; j < 22; j++) //oszlop
                {
                    if (i < 2 || i > 9 || j < 2 || j > 19) //k�ls� k�t sor keret
                    {
                        tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.KERET));
                    }
                    else //a t�bbi �res h�tt�r
                    {
                        tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.EMPTY));
                    }
                }
            }

            GameLogic gameLogic = new GameLogic(22, 12, 36);
            gameLogic.MoveEveryMovableObject(tesztPalya, 0, 0);

            Assert.AreEqual(12 * 22, tesztPalya.Count);
            Assert.AreEqual(0, gameLogic.Currentrulesofthegame.Count);
        }

        [Test]
        public void ThereIsOnlyOneRuleOnTheMap_Test()
        {
            List<MapObject> tesztPalya = new List<MapObject>();
            for (int i = 0; i < 12; i++) //sor
            {
                for (int j = 0; j < 22; j++) //oszlop
                {
                    if (i < 2 || i > 9 || j < 2 || j > 19) //k�ls� k�t sor keret
                    {
                        tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.KERET));
                    }
                    else if (i == 6 && (j == 9 || j == 10 || j == 11)) //lesz egy szab�ly
                    {
                        if (j == 9)
                        {
                            tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.BABA_WORD));
                        }
                        if (j == 10)
                        {
                            tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.IS));
                        }
                        if (j == 11)
                        {
                            tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.YOU));
                        }
                    }
                    else //a t�bbi �res h�tt�r
                    {
                        tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.EMPTY));
                    }
                }
            }

            GameLogic gameLogic = new GameLogic(22, 12, 36);
            gameLogic.MoveEveryMovableObject(tesztPalya, 0, 0);

            Assert.AreEqual(1, gameLogic.Currentrulesofthegame.Count);
        }

        [Test]
        public void ThereAreMultipleRulesOnTheMap_Test()
        {
            List<MapObject> tesztPalya = new List<MapObject>();
            for (int i = 0; i < 12; i++) //sor
            {
                for (int j = 0; j < 22; j++) //oszlop
                {
                    if (i < 2 || i > 9 || j < 2 || j > 19) //k�ls� k�t sor keret
                    {
                        tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.KERET));
                    }
                    else if (i == 6 && (j == 9 || j == 10 || j == 11)) //lesz egy szab�ly
                    {
                        if (j == 9)
                        {
                            tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.BABA_WORD));
                        }
                        if (j == 10)
                        {
                            tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.IS));
                        }
                        if (j == 11)
                        {
                            tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.YOU));
                        }
                    }
                    else if (i == 4 && (j == 9 || j == 10 || j == 11)) //lesz egy szab�ly
                    {
                        if (j == 9)
                        {
                            tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.FLAG_WORD));
                        }
                        if (j == 10)
                        {
                            tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.IS));
                        }
                        if (j == 11)
                        {
                            tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.WIN));
                        }
                    }
                    else if (i == 8 && (j == 9 || j == 10 || j == 11)) //lesz egy szab�ly
                    {
                        if (j == 9)
                        {
                            tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.WALL_WORD));
                        }
                        if (j == 10)
                        {
                            tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.IS));
                        }
                        if (j == 11)
                        {
                            tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.STOP));
                        }
                    }
                    else //a t�bbi �res h�tt�r
                    {
                        tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.EMPTY));
                    }
                }
            }

            GameLogic gameLogic = new GameLogic(22, 12, 36);
            gameLogic.MoveEveryMovableObject(tesztPalya, 0, 0);

            Assert.AreEqual(3, gameLogic.Currentrulesofthegame.Count);
        }

        [Test]
        public void HorizontalRuleDetection_Test()
        {
            List<MapObject> tesztPalya = new List<MapObject>();
            for (int i = 0; i < 12; i++) //sor
            {
                for (int j = 0; j < 22; j++) //oszlop
                {
                    if (i < 2 || i > 9 || j < 2 || j > 19) //k�ls� k�t sor keret
                    {
                        tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.KERET));
                    }
                    else if (i == 6 && (j == 9 || j == 10 || j == 11)) //lesz egy szab�ly
                    {
                        if (j == 9)
                        {
                            tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.BABA_WORD));
                        }
                        if (j == 10)
                        {
                            tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.IS));
                        }
                        if (j == 11)
                        {
                            tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.YOU));
                        }
                    }
                    else //a t�bbi �res h�tt�r
                    {
                        tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.EMPTY));
                    }
                }
            }

            GameLogic gameLogic = new GameLogic(22, 12, 36);
            gameLogic.MoveEveryMovableObject(tesztPalya, 0, 0);

            Assert.AreEqual(MapObjectEnum.BABA_WORD, gameLogic.Currentrulesofthegame[0].WORD);
            Assert.AreEqual(MapObjectEnum.IS, gameLogic.Currentrulesofthegame[0].Operator);
            Assert.AreEqual(MapObjectEnum.YOU, gameLogic.Currentrulesofthegame[0].Property);
            Assert.AreEqual(VerticalHorizontalEnum.HORIZONTAL, gameLogic.Currentrulesofthegame[0].VHE);
        }

        [Test]
        public void VerticalRuleDetection_Test()
        {
            List<MapObject> tesztPalya = new List<MapObject>();
            for (int i = 0; i < 12; i++) //sor
            {
                for (int j = 0; j < 22; j++) //oszlop
                {
                    if (i < 2 || i > 9 || j < 2 || j > 19) //k�ls� k�t sor keret
                    {
                        tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.KERET));
                    }
                    else if (j == 11 && (i == 4 || i == 5 || i == 6)) //lesz egy szab�ly
                    {
                        if (i == 4)
                        {
                            tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.BABA_WORD));
                        }
                        if (i == 5)
                        {
                            tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.IS));
                        }
                        if (i == 6)
                        {
                            tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.YOU));
                        }
                    }
                    else //a t�bbi �res h�tt�r
                    {
                        tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.EMPTY));
                    }
                }
            }

            GameLogic gameLogic = new GameLogic(22, 12, 36);
            gameLogic.MoveEveryMovableObject(tesztPalya, 0, 0);

            Assert.AreEqual(MapObjectEnum.BABA_WORD, gameLogic.Currentrulesofthegame[0].WORD);
            Assert.AreEqual(MapObjectEnum.IS, gameLogic.Currentrulesofthegame[0].Operator);
            Assert.AreEqual(MapObjectEnum.YOU, gameLogic.Currentrulesofthegame[0].Property);
            Assert.AreEqual(VerticalHorizontalEnum.VERTICAL, gameLogic.Currentrulesofthegame[0].VHE);
        }

        [Test]
        public void RuleObjectBehaviour_Test()
        {
            List<MapObject> tesztPalya = new List<MapObject>();
            for (int i = 0; i < 12; i++) //sor
            {
                for (int j = 0; j < 22; j++) //oszlop
                {
                    if (i < 2 || i > 9 || j < 2 || j > 19) //k�ls� k�t sor keret
                    {
                        tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.KERET));
                    }
                    else if (i == 6 && (j == 9 || j == 10 || j == 11)) //lesz egy szab�ly
                    {
                        if (j == 9)
                        {
                            tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.BABA_WORD));
                        }
                        if (j == 10)
                        {
                            tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.IS));
                        }
                        if (j == 11)
                        {
                            tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.YOU));
                        }
                    }
                    else //a t�bbi �res h�tt�r
                    {
                        tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.EMPTY));
                    }
                }
            }

            GameLogic gameLogic = new GameLogic(22, 12, 36);
            gameLogic.MoveEveryMovableObject(tesztPalya, 0, 0);

            Assert.AreEqual(MapObjectEnum.PUSH, tesztPalya[141].RelevantBehaviors[0]); //BABA_WORD
            Assert.AreEqual(MapObjectEnum.PUSH, tesztPalya[142].RelevantBehaviors[0]); //IS
            Assert.AreEqual(MapObjectEnum.PUSH, tesztPalya[143].RelevantBehaviors[0]); //YOU
        }

        [Test]
        public void DefeatInCaseOfEliminationOf_SOMETHING_IS_YOU_Rules_Test()
        {
            List<MapObject> tesztPalya = new List<MapObject>();
            for (int i = 0; i < 12; i++) //sor
            {
                for (int j = 0; j < 22; j++) //oszlop
                {
                    if (i < 2 || i > 9 || j < 2 || j > 19) //k�ls� k�t sor keret
                    {
                        tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.KERET));
                    }
                    else if (i == 6 && (j == 9 || j == 10 || j == 11)) //lesz egy szab�ly
                    {
                        if (j == 9)
                        {
                            tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.BABA_WORD));
                        }
                        if (j == 10)
                        {
                            tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.IS));
                        }
                        if (j == 11)
                        {
                            tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.YOU));
                        }
                    }
                    else if (i == 5 && j == 11) //BABA 
                    {
                        tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.BABA));
                    }
                    else //a t�bbi �res h�tt�r
                    {
                        tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.EMPTY));
                    }
                }
            }

            GameLogic gameLogic = new GameLogic(22, 12, 36);
            gameLogic.MoveEveryMovableObject(tesztPalya, 0, 1); //BABA lefel� mozog egyet �s a BABA IS YOU szab�ly YOU blokkj�t letolja, �gy mikor lefutna a k�vetkez� mozgat�s v�get �r a j�t�k

            Assert.AreEqual(1, gameLogic.MoveEveryMovableObject(tesztPalya, 0, 1));
        }

        [Test]
        public void PushingMultipleMapObjectsHorizontally_Test()
        {
            List<MapObject> tesztPalya = new List<MapObject>();
            for (int i = 0; i < 12; i++) //sor
            {
                for (int j = 0; j < 22; j++) //oszlop
                {
                    if (i < 2 || i > 9 || j < 2 || j > 19) //k�ls� k�t sor keret
                    {
                        tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.KERET));
                    }
                    else if (i == 6 && (j == 9 || j == 10 || j == 11)) //lesz egy szab�ly
                    {
                        if (j == 9)
                        {
                            tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.BABA_WORD));
                        }
                        if (j == 10)
                        {
                            tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.IS));
                        }
                        if (j == 11)
                        {
                            tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.YOU));
                        }
                    }
                    else if (i == 6 && j == 8) //BABA 
                    {
                        tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.BABA));
                    }
                    else //a t�bbi �res h�tt�r
                    {
                        tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.EMPTY));
                    }
                }
            }

            GameLogic gameLogic = new GameLogic(22, 12, 36);

            //eltol�s el�tti poz�ci�k
            Assert.AreEqual(MapObjectEnum.BABA, tesztPalya[140].MyProperty);
            Assert.AreEqual(MapObjectEnum.BABA_WORD, tesztPalya[141].MyProperty);
            Assert.AreEqual(MapObjectEnum.IS, tesztPalya[142].MyProperty);
            Assert.AreEqual(MapObjectEnum.YOU, tesztPalya[143].MyProperty);

            gameLogic.MoveEveryMovableObject(tesztPalya, 1, 0); //BABA eltolja jobbra a horizont�lis szab�lyt

            //eltol�s ut�ni poz�ci�k
            Assert.AreEqual(MapObjectEnum.BABA, tesztPalya[141].MyProperty);
            Assert.AreEqual(MapObjectEnum.BABA_WORD, tesztPalya[142].MyProperty);
            Assert.AreEqual(MapObjectEnum.IS, tesztPalya[143].MyProperty);
            Assert.AreEqual(MapObjectEnum.YOU, tesztPalya[144].MyProperty);
        }

        [Test]
        public void PushingMultipleMapObjectsVertically_Test()
        {
            List<MapObject> tesztPalya = new List<MapObject>();
            for (int i = 0; i < 12; i++) //sor
            {
                for (int j = 0; j < 22; j++) //oszlop
                {
                    if (i < 2 || i > 9 || j < 2 || j > 19) //k�ls� k�t sor keret
                    {
                        tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.KERET));
                    }
                    else if (j == 11 && (i == 4 || i == 5 || i == 6)) //lesz egy szab�ly
                    {
                        if (i == 4)
                        {
                            tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.BABA_WORD));
                        }
                        if (i == 5)
                        {
                            tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.IS));
                        }
                        if (i == 6)
                        {
                            tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.YOU));
                        }
                    }
                    else if (j == 11 && i == 3) //BABA 
                    {
                        tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.BABA));
                    }
                    else //a t�bbi �res h�tt�r
                    {
                        tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.EMPTY));
                    }
                }
            }

            GameLogic gameLogic = new GameLogic(22, 12, 36);

            //eltol�s el�tti poz�ci�k
            Assert.AreEqual(MapObjectEnum.BABA, tesztPalya[77].MyProperty);
            Assert.AreEqual(MapObjectEnum.BABA_WORD, tesztPalya[99].MyProperty);
            Assert.AreEqual(MapObjectEnum.IS, tesztPalya[121].MyProperty);
            Assert.AreEqual(MapObjectEnum.YOU, tesztPalya[143].MyProperty);

            gameLogic.MoveEveryMovableObject(tesztPalya, 0, 1); //BABA eltolja lefel� a vertik�lis szab�lyt

            //eltol�s ut�ni poz�ci�k
            Assert.AreEqual(MapObjectEnum.BABA, tesztPalya[99].MyProperty);
            Assert.AreEqual(MapObjectEnum.BABA_WORD, tesztPalya[121].MyProperty);
            Assert.AreEqual(MapObjectEnum.IS, tesztPalya[143].MyProperty);
            Assert.AreEqual(MapObjectEnum.YOU, tesztPalya[165].MyProperty);
        }

        [Test]
        public void VictoryOnThisMap_Test()
        {
            List<MapObject> tesztPalya = new List<MapObject>();
            for (int i = 0; i < 12; i++) //sor
            {
                for (int j = 0; j < 22; j++) //oszlop
                {
                    if (i < 2 || i > 9 || j < 2 || j > 19) //k�ls� k�t sor keret
                    {
                        tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.KERET));
                    }
                    else if (i == 6 && (j == 9 || j == 10 || j == 11)) //lesz egy szab�ly
                    {
                        if (j == 9)
                        {
                            tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.BABA_WORD));
                        }
                        if (j == 10)
                        {
                            tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.IS));
                        }
                        if (j == 11)
                        {
                            tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.YOU));
                        }
                    }
                    else if (i == 4 && (j == 9 || j == 10 || j == 11)) //lesz egy szab�ly
                    {
                        if (j == 9)
                        {
                            tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.FLAG_WORD));
                        }
                        if (j == 10)
                        {
                            tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.IS));
                        }
                        if (j == 11)
                        {
                            tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.WIN));
                        }
                    }
                    else if (i == 3 && j == 3) //BABA 
                    {
                        tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.BABA));
                    }
                    else if (i == 3 && j == 4) //z�szl�
                    {
                        tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.FLAG));
                    }
                    else //a t�bbi �res h�tt�r
                    {
                        tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.EMPTY));
                    }
                }
            }

            GameLogic gameLogic = new GameLogic(22, 12, 36);

            Assert.AreEqual(2, gameLogic.MoveEveryMovableObject(tesztPalya, 1, 0));
        }

        [Test]
        public void Jump_Test()
        {
            List<MapObject> tesztPalya = new List<MapObject>();
            for (int i = 0; i < 12; i++) //sor
            {
                for (int j = 0; j < 22; j++) //oszlop
                {
                    if (i < 2 || i > 9 || j < 2 || j > 19) //k�ls� k�t sor keret
                    {
                        tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.KERET));
                    }
                    else if (i == 6 && (j == 9 || j == 10 || j == 11)) //lesz egy szab�ly
                    {
                        if (j == 9)
                        {
                            tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.BABA_WORD));
                        }
                        if (j == 10)
                        {
                            tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.IS));
                        }
                        if (j == 11)
                        {
                            tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.YOU));
                        }
                    }
                    else if (i == 3 && j == 3) //BABA 
                    {
                        tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.BABA));
                    }
                    else if (i == 3 & (j == 4 || j == 5)) //GRASS
                    {
                        tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.GRASS));
                    }
                    else //a t�bbi �res h�tt�r
                    {
                        tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.EMPTY));
                    }
                }
            }

            GameLogic gameLogic = new GameLogic(22, 12, 36);
            gameLogic.MoveEveryMovableObject(tesztPalya, 1, 0); //BABA egyet l�pne jobbra, de mivel mellette k�zvetlen f� van, �s az �tugorhat�, ez�rt �tugorja

            //a f� marad a hely�n, BABA �tugorja azt
            Assert.AreEqual(MapObjectEnum.GRASS, tesztPalya[70].MyProperty);
            Assert.AreEqual(MapObjectEnum.GRASS, tesztPalya[71].MyProperty);
            Assert.AreEqual(MapObjectEnum.BABA, tesztPalya[72].MyProperty);
        }

        [Test]
        public void FrameStops_Test()
        {
            List<MapObject> tesztPalya = new List<MapObject>();
            for (int i = 0; i < 12; i++) //sor
            {
                for (int j = 0; j < 22; j++) //oszlop
                {
                    if (i < 2 || i > 9 || j < 2 || j > 19) //k�ls� k�t sor keret
                    {
                        tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.KERET));
                    }
                    else if (i == 6 && (j == 9 || j == 10 || j == 11)) //lesz egy szab�ly
                    {
                        if (j == 9)
                        {
                            tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.BABA_WORD));
                        }
                        if (j == 10)
                        {
                            tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.IS));
                        }
                        if (j == 11)
                        {
                            tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.YOU));
                        }
                    }
                    else if (i == 2 && j == 2) //BABA a bal fels� sarokban van a keret mellett k�zvetlen
                    {
                        tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.BABA));
                    }
                    else //a t�bbi �res h�tt�r
                    {
                        tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.EMPTY));
                    }
                }
            }

            GameLogic gameLogic = new GameLogic(22, 12, 36);
            gameLogic.MoveEveryMovableObject(tesztPalya, -1, 0); //BABA egyet l�pne balra, de mivel mellette k�zvetlen keret van, ami meg�ll�tja, ez�rt marad a hely�n

            Assert.AreEqual(MapObjectEnum.BABA, tesztPalya[46].MyProperty);

        }
    }
}
