﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouAreBaba.Model
{
    public class RuleObject
    {
        public RuleObject(MapObjectEnum wORD, MapObjectEnum @operator, MapObjectEnum property, VerticalHorizontalEnum vhe)
        {
            WORD = wORD;
            Operator = @operator;
            Property = property;
            VHE = vhe;
        }
        public MapObjectEnum WORD { get; set; }
        public MapObjectEnum Operator { get; set; }
        public MapObjectEnum Property { get; set; }
        public VerticalHorizontalEnum VHE { get; set; }
    }
}
