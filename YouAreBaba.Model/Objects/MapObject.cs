﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace YouAreBaba.Model
{
    public class MapObject
    {
        private Rect area;
        public MapObject(int x, int y, double width, double height, MapObjectEnum thisenum)
        {
            this.Dx = x;
            this.Dy = y;
            this.area = new Rect(Dx, Dy, width, height);
            this.MyProperty = thisenum;
            this.RelevantBehaviors = new List<MapObjectEnum>();
            this.MovedInThisTick = false;
        }
        /// <summary>
        /// Gets or sets the degree of change on the x-axis
        /// </summary>
        public int Dx { get; set; }
        /// <summary>
        /// Gets or sets the degree of change on the y-axis
        /// </summary>
        public int Dy { get; set; }
        /// <summary>
        /// Gets or sets the rectangle that represents the shape
        /// </summary>
        public Rect Area { get => this.area; set => this.area = value; }
        /// <summary>
        /// Moves shape by dx on the x-axis and dy on the y-axis
        /// </summary>
        /// <param name="dx">Degree of movement on the x-axis</param>
        /// <param name="dy">Degree of mevement on the y-axis</param>
        public void Move(double dx, double dy)
        {
            this.area.X += dx;
            this.area.Y += dy;
        }
        public MapObjectEnum MyProperty { get; set; }
        public List<MapObjectEnum> RelevantBehaviors { get; set; }
        public bool MovedInThisTick { get; set; }
        public void MoveInThisTick()
        {
            this.MovedInThisTick = true;
        }
        public void NotMoveInThisTick()
        {
            this.MovedInThisTick = false;
        }
    }
}
