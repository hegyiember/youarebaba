﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace YouAreBaba.Model
{
    public class FluidButton
    {
        public FluidButton(string content, double leftPos, double topPos, double productId)
        {
            Content = content;
            LeftPos = leftPos;
            TopPos = topPos;
            ProductId = productId;
            Background = Brushes.Transparent;
        }
        public Brush Background { get; set; }
        public string Content { get; set; }
        public double LeftPos { get; set; }
        public double TopPos { get; set; }
        public double ProductId { get; set; }
        public Thickness ControlMargin { get { return new Thickness { Left = this.LeftPos, Top = this.TopPos }; }  }
    }
}
