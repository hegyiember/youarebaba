﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouAreBaba.Model
{
    public enum MapObjectEnum
    {
        // MapObjectEnum
        // https://babaiswiki.fandom.com/wiki/Category:Nouns
            BABA,
            BABA_WORD,
            EMPTY, // fekete háttér, amin mozoghatsz !!! vagyis ezt cska tároljuk a mátrixban de a helyén semmit nem rajzolunk ki! (alapból a háttér színe lesz helyén)
            EMPTY_WORD, // https://babaiswiki.fandom.com/wiki/EMPTY
            WALL, // 
            KERET, 
            WALL_WORD,
            WATER, // https://babaiswiki.fandom.com/wiki/WATER */
            WATER_WORD,
            SKULL, // https://babaiswiki.fandom.com/wiki/SKULL
            SKULL_WORD,
            FENCE, // https://babaiswiki.fandom.com/wiki/FENCE
            FENCE_WORD,
            HEDGE, // https://babaiswiki.fandom.com/wiki/HEDGE
            HEDGE_WORD,
            TREE, // https://babaiswiki.fandom.com/wiki/TREE
            TREE_WORD,
            FLAG, // https://babaiswiki.fandom.com/wiki/FLAG
            FLAG_WORD,
            KEY, // https://babaiswiki.fandom.com/wiki/KEY
            KEY_WORD,
            GRASS, // https://babaiswiki.fandom.com/wiki/Grass_Yard
            GRASS_WORD,
            KEKE, // https://babaiswiki.fandom.com/wiki/KEKE
            KEKE_WORD,
            ICE, // https://babaiswiki.fandom.com/wiki/ICE
            ICE_WORD,
            BELT, // https://babaiswiki.fandom.com/wiki/BELT
            BELT_WORD,
            BOX, // https://babaiswiki.fandom.com/wiki/BOX
            BOX_WORD,
            LEVEL_WORD, // https://babaiswiki.fandom.com/wiki/LEVEL
            LAVA, // https://babaiswiki.fandom.com/wiki/LAVA
            LAVA_WORD,
            DOOR, // https://babaiswiki.fandom.com/wiki/DOOR
            DOOR_WORD,
            ROCK, // https://babaiswiki.fandom.com/wiki/ROCK
            ROCK_WORD,
            NEEDTOMOVE, // Pixel moving helper flag
            ONLYMOVEONCE,

        // OperatorsEnum
        // https://babaiswiki.fandom.com/wiki/Category:Operators
        IS, // BABA IS WALL : Turns BABA into WALL.
            // HAS, // Makes an object spawn another object when it is destroyed. 
                 // BABA HAS WALL : When BABA is destroyed, it will be replaced by WALL.
            // AND, // BABA AND WALL IS YOU : Both BABA and any WALLS are controllable.
            // MAKE, // BABA MAKE WALL : ha lép egyet a baba akkor az ellépett helyére lespawnol egy falat
            // ON, // BABA ON WALL IS WATER : BABA turns into WATER when overlapping with WALL.
            // NEAR, // BABA NEAR WALL IS YOU : In order to control BABA, it must be next to a WALL.

        // PropertiesEnum
            // https://babaiswiki.fandom.com/wiki/Category:Properties
            YOU, // Something that exists on the board has to be YOU or you fail the level. Examples: BABA IS YOU, WALL IS YOU, LEAF IS YOU, etc.
            WIN, // Either YOU have to stand on whichever item is WIN or whichever item you are has to also be WIN. This is how you clear a level. Examples: BABA IS YOU and FLAG IS WIN — stand on the flag to win. Or BABA IS YOU AND BABA IS WIN.
            PUSH, // This means the object can be pushed. Example: If ROCK IS PUSH, you can push rocks.
            //PULL, // This is the opposite of PUSH. You can pull the item behind you. If TEXT IS PULL, you can both push and pull the text. Examples: If KEY IS PULL, you can pull the key behind you. If TEXT AND KEY IS PULL, you can pull the text and the key behind you in a long line.
            STOP, // This item prevents you from moving yourself or anything else through it. Example: If WALL IS STOP, you can’t go through walls or push any words or other objects through it.
            SINK, // Touching an item that’s SINK will make both you and it disappear. Examples: WATER IS SINK — If you touch water, you will both disappear. Or if BABA IS SINK, anything you touch will cause both of you to disappear.
            DEFEAT, // If you touch this item, you disappear. If there was only one of you, you also fail the level. Example: If SKULL IS DEFEAT, then you’ll die when you touch a skull.
            MOVE, // This makes an object move on its own in the direction it’s facing. If it hits an obstacle, it will turn around and walk the other way. You can make it move without you by pressing the spacebar (or A on Switch) to wait. Paired with YOU, it makes you do a double-move that allows you to walk over thinks that normally would kill you. Example: If KEKE IS MOVE, then Keke will walk on its own as you move or as you wait.
            SHUT, OPEN, //  If something is SHUT, it will disappear when something that is OPEN touches it, and so will the other object. Usually SHUT comes with STOP, but pay close attention, as SHUT on its own doesn’t stop you. Example: If DOOR IS SHUT and KEY IS OPEN, then pushing the key onto the door will make both the door and key disappear.
            WEAK, // You can jump over
            SWAP, // f something is SWAP, it will change places with anything it touches. Example: If BABA IS SWAP, then Baba can’t land on the flag, because he’ll swap with it. He’ll also swap with any text he runs into.
    }
}
