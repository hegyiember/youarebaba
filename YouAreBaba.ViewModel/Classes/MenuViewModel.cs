﻿using CommonServiceLocator;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using YouAreBaba.Logic;
using YouAreBaba.Model;

namespace YouAreBaba.ViewModel
{
    public class MenuViewModel : ViewModelBase, IMenuViewModel
    {
        private int switchView;
        public int SwitchView
        {
            get { return switchView; }
            set { Set(ref switchView, value); }
        }
        private bool gameviewhasbeenloaded;
        public bool GameViewhasbeenloaded
        {
            get { return gameviewhasbeenloaded; }
            set { Set(ref gameviewhasbeenloaded, value); }
        }
        private List<int[]> AllMapsIDAndSizes { get; set; }
        private List<int[]> AllPossibleMapCo { get; set; }
        public BitmapImage Logo { get; }
        public bool MenuOrGame;
        private int whichback;
        private List<BitmapImage> back_menu;
        private List<BitmapImage> back_game;
        private BitmapImage back;
        public BitmapImage Back
        {
            get { return back; }
            set { Set(ref back, value); }
        }
        private DispatcherTimer timer;
        public BitmapImage Help { get; }
        public List<int> FromSavedGameFinishedMap { get; set; }
        private int lastPlayedMap;
        public int LastPlayedMap
        {
            get { return lastPlayedMap; }
            set { Set(ref lastPlayedMap, value); }
        }
        private SolidColorBrush gameBackgroudColor;
        public SolidColorBrush GameBackgroudColor
        {
            get { return gameBackgroudColor; }
            set { Set(ref gameBackgroudColor, value); }
        }
        private ObservableCollection<FluidButton> myButtons;
        public ObservableCollection<FluidButton> MyButtons
        {
            get { return myButtons; }
            set { Set(ref myButtons, value); }
        }
        private bool canLoad;
        public bool CanLoad
        {
            get { return canLoad; }
            set { Set(ref canLoad, value); }
        }
        private int positionHeight;
        public int PositionHeight
        {
            get { return positionHeight; }
            set { Set(ref positionHeight, value); }
        }
        private int positionWidth;
        public int PositionWidth
        {
            get { return positionWidth; }
            set { Set(ref positionWidth, value); }
        }

        // HIGHSCORE ******************************************************
        private string gamerName;
        public string GamerName
        {
            get { return gamerName; }
            set { Set(ref gamerName, value); }
        }
        private List<string> gamerList;
        public List<string> GamerList
        {
            get { return gamerList; }
            set { Set(ref gamerList, value); }
        }

        // HIGHSCORE ******************************************************

        public IMenuLogic Logic
        {
            get;
            //get { return ServiceLocator.Current.GetInstance<IMenuLogic>(); }
        }

        public MenuViewModel()
        {
            this.positionHeight = 1080 / 2 - 300;
            this.positionWidth = 1920 / 2 - 400;

            this.Logic = new MenuLogic();
            this.GameViewhasbeenloaded = false;

            this.SwitchView = 5;
            this.AllMapsIDAndSizes = this.ScanMapSizes();

            this.FromSavedGameFinishedMap = this.Logic.GetAllVictoryMapNumberByUser(this.GamerName); //this.Logic.LoadLastPlayedMap();

            this.back_game = new List<BitmapImage>();
            this.back_menu = new List<BitmapImage>();
            this.back_menu.Add(new BitmapImage(new Uri(@"../../Menu/menu/0.png", UriKind.Relative)));
            this.back_menu.Add(new BitmapImage(new Uri(@"../../Menu/menu/1.png", UriKind.Relative)));
            this.back_menu.Add(new BitmapImage(new Uri(@"../../Menu/menu/2.png", UriKind.Relative)));
            this.back_game.Add(new BitmapImage(new Uri(@"../../Menu/game/0.png", UriKind.Relative)));
            this.back_game.Add(new BitmapImage(new Uri(@"../../Menu/game/1.png", UriKind.Relative)));
            this.back_game.Add(new BitmapImage(new Uri(@"../../Menu/game/2.png", UriKind.Relative)));
            this.whichback = 0;
            this.MenuOrGame = true;
            this.timer = new DispatcherTimer();
            this.timer.Interval = TimeSpan.FromMilliseconds(200);
            this.timer.Tick += this.Timer_Back;
            this.timer.Start();

            this.Logo = new BitmapImage(new Uri(@"../../Menu/logo.gif", UriKind.Relative));
            this.Help = new BitmapImage(new Uri(@"../../Menu/Help.png", UriKind.Relative));
            this.MyButtons = new ObservableCollection<FluidButton>();
            this.AllPossibleMapCo = AllPossibleMapCoordinates();


            for (int i=0; i< AllMapsIDAndSizes.Count; i++)
            {
                this.MyButtons.Add(new FluidButton(
                    AllMapsIDAndSizes[i][0] < 10 ? "0" + AllMapsIDAndSizes[i][0].ToString() : AllMapsIDAndSizes[i][0].ToString()
                    , AllPossibleMapCo[i][0] - 15, AllPossibleMapCo[i][1] -15, AllMapsIDAndSizes[i][0]));
            }

            for (int i = 0; i < MyButtons.Count; i++)
            {
                if(FromSavedGameFinishedMap.Contains(i)) MyButtons[i].Background = new SolidColorBrush(Colors.Green);
            }

        }

        private void Timer_Back(object sender, EventArgs e)
        {
            if (this.whichback < 2) this.whichback++;
            else this.whichback = 0;

            if (this.MenuOrGame) this.Back = this.back_menu[whichback];
            else this.Back = this.back_game[whichback];
        }

        public void ChangeLastPlayedMapButtonToRed()
        {
            this.Logic.ChangeLastPlayedMapButtonToRed(this.MyButtons[this.LastPlayedMap]);
        }
        public void ChangeLastPlayedMapButtonToGreen()
        {
            this.Logic.ChangeLastPlayedMapButtonToGreen(this.MyButtons[this.LastPlayedMap]);
        }
        public void ChangeMapButtonToGreen(int d)
        {
            if(d <= this.AllPossibleMapCo.Count()) if(d < 14) this.Logic.ChangeMapButtonToGreen(this.MyButtons[d]);
        }
        public void ChangeVictoriousPlayedMapButtonToGreen()
        {
            var listofalreadyfinishedmaps = this.Logic.GetAllVictoryMapNumberByUser(this.GamerName);
            foreach (var x in listofalreadyfinishedmaps)  this.ChangeMapButtonToGreen(x);
        }
        public void ChangeAllPlayedMapButtonToNothing()
        {
            foreach(var x in this.MyButtons)
            {
                x.Background = null;
            }
        }
        private List<int[]> AllPossibleMapCoordinates()
        {
            var temp = new List<int[]>
            {
                new int[] { 245,241 },
                new int[] { 422,284 },
                new int[] { 411,132 },
                new int[] { 462,453 },
                new int[] { 646,493 },
                new int[] { 572,310 },
                new int[] { 614,156 },
                new int[] { 778,253 },
                new int[] { 765,424 },
                new int[] { 377,381 },
                new int[] { 337,482 },
                new int[] { 203,475 },
                new int[] { 157,370 },
                new int[] { 277,144 },
                new int[] { 152,190 },
                new int[] { 348,256 },
                new int[] { 279,518 },
                new int[] { 452,370 },
                new int[] { 575,473 },
                new int[] { 649,269 },
                new int[] { 554,219 }
            };
            return temp;
        }
        public void CleanLastPlayedMap(string name)
        {
            this.Logic.CleanLastPlayedMap(name);
        }
        public List<int[]> ScanMapSizes()
        {
            return this.Logic.ScanMapSizes();
        }

        // HIGHSCORE ******************************************************
        public List<Highscore> Get()
        {
            return this.Logic.Get();
        }

        public bool Add(string name, int mapid, int time)
        {
            if (mapid <= this.AllMapsIDAndSizes.Count) return this.Logic.Add(name, mapid, time);
            else return false;
        }

        public bool DeleteAllRecord(string name)
        {
            return this.Logic.DeleteAllRecord(name);
        }

        public List<int> GetAllVictoryMapNumberByUser(string name)
        {
            return this.Logic.GetAllVictoryMapNumberByUser(name);
        }

        public bool CheckUserExists(string name)
        {
            return this.Logic.CheckUserExists(name);
        }

        public void SetGAmeBackgroundColor(int id)
        {
            switch (this.AllMapsIDAndSizes[id][3])
            {
                case 1: this.GameBackgroudColor = Brushes.DarkSlateBlue; break;
                case 2: this.GameBackgroudColor = Brushes.DarkBlue; break;
                case 3: this.GameBackgroudColor = Brushes.DarkGoldenrod; break;
                case 4: this.GameBackgroudColor = Brushes.DarkOrange; break;
                case 5: this.GameBackgroudColor = Brushes.DarkSlateGray; break;
                default: this.GameBackgroudColor = Brushes.DarkSlateBlue; break;
            }
        }
        public bool CanThisMapBePlayedByUser(int mapid)
        {
            return this.Logic.CanThisMapBePlayedByUser(this.GamerName, mapid);
        }
        public void SetMapForGameplay(int mapid, int objectPixel)
        {
            //this.PositionHeight = 1080 / 2 - ((this.AllMapsIDAndSizes[mapid][2] * objectPixel) / 2);
            //this.PositionWidth = 1920 / 2 - ((this.AllMapsIDAndSizes[mapid][1] * objectPixel) / 2);
            this.PositionWidth = 21 * 24 + 8;
            this.PositionHeight = 8 * 24 + 5;
        }
        // HIGHSCORE ******************************************************
    }
}
