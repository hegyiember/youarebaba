﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouAreBaba.Logic;

namespace YouAreBaba.ViewModel
{
    public class UserViewModel : ViewModelBase, IUserViewModel
    {
        private string chosenUser;
        private List<string> usernames;
        private string fromListChosenUser;
        private bool boolfromListChosenUser;
        private bool boolchosenUser;
        public string ChosenUser { get { return chosenUser; } set { Set(ref chosenUser, value); } }
        public List<string> Usernames { get { return usernames; } set { Set(ref usernames, value); } }
        public string FromListChosenUser { get { return fromListChosenUser; } set { Set(ref fromListChosenUser, value); } }
        public bool FromListChosenUserBool { get { return boolfromListChosenUser; } set { Set(ref boolfromListChosenUser, value); } }
        public bool BoolchosenUser { get { return boolchosenUser; } set { Set(ref boolchosenUser, value); } }

        public IUserLogic Logic
        {
            get;
        }
        public UserViewModel()
        {
            this.ChosenUser = "";
            this.FromListChosenUser = "";
            this.FromListChosenUserBool = false;
            this.BoolchosenUser = false;
            this.Logic = new UserLogic();
            this.Usernames = this.Logic.GetOnlyNmes();
            this.FromListChosenUser = this.Usernames[0];

        }
        public bool CheckUserExists()
        {
            return this.Logic.CheckUserExists(ChosenUser);
        }
        public bool Add(string name, int mapid, int time)
        {
            return this.Logic.Add(name, mapid, time);
        }
    }
}
