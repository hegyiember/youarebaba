﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using YouAreBaba.Model;
using YouAreBaba.Logic;
using System.Windows;

namespace YouAreBaba.ViewModel
{
    public class GameViewModel : IGameViewModel
    {
        public IGameLogic Logic { get; set; }
        public List<List<MapObject>> AllMaps { get; set; }
        public List<MapObject> CurrentlyPlayedMap { get; set; }
        public List<MapObject> PrevoiusmoveCurrentlyPlayedMap { get; set; }
        public List<BitmapImage> MapObjectImages { get; set; }
        public DateTime Started { get; set; }
        public int CurrentMap { get; set; }
        private int ObjectPixel{ get; set; }

        public GameViewModel(int width, int height, int ObjectPixel, int currentmap)
        {

            this.ObjectPixel = ObjectPixel;
            this.Logic = new GameLogic(width,height, this.ObjectPixel);
            this.AllMaps = Logic.ScanAllMaps();
            this.CurrentlyPlayedMap = AllMaps[currentmap];
            this.PrevoiusmoveCurrentlyPlayedMap = CurrentlyPlayedMap;
            this.MapObjectImages = new List<BitmapImage>();
            this.Started = DateTime.Now;
            this.CurrentMap = currentmap;
        }
        public void AddSaveGame()
        {
            this.Logic.AddSaveGame(CurrentMap, CurrentlyPlayedMap);
        }
        public void LoadGame()
        {
            var temp = this.Logic.LoadGame();
            for(int inx= 0; inx < this.CurrentlyPlayedMap.Count(); inx++)
            {
                this.CurrentlyPlayedMap[inx] = temp[inx];
            }
        }

        public int Move(int x, int y)
        {
            this.PrevoiusmoveCurrentlyPlayedMap = CurrentlyPlayedMap;
            return this.Logic.MoveEveryMovableObject(CurrentlyPlayedMap, x, y);
        }
        public void MoveSmoothlyPixels(int x, int y, int thismuchmoved)
        {
            this.Logic.MoveSmoothlyPixels(this.CurrentlyPlayedMap, x, y, thismuchmoved);
        }
        public List<string> ScanAllPictureNames()
        {
            return Logic.ScanAllPictureNames();
        }

        public Rect GetCurrentlyPlayedMapObjectByCoordinates(int i, int j)
        {
            foreach (var s in this.CurrentlyPlayedMap)
            {
                if (s.Dx == i * ObjectPixel && s.Dy == j * ObjectPixel)
                {
                    return s.Area;
                }
            }
            return new Rect(0, 0, ObjectPixel, ObjectPixel);
        }

        public string GetEmumNameOfCurrentlyPlayedMapObjectByCoordinates(int i, int j)
        {
            foreach (var s in this.CurrentlyPlayedMap)
            {
                if (s.Dx == i * this.ObjectPixel && s.Dy == j * this.ObjectPixel)
                {
                    return s.MyProperty.ToString();
                }
            }
            return "EMPTY";
        }

        public List<string> ScanAllFramesOfGif(string temp)
        {
            return this.Logic.ScanAllFramesOfGif(temp);
        }
    }
}
