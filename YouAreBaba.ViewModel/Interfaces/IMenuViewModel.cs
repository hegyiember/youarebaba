﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using YouAreBaba.Logic;
using YouAreBaba.Model;

namespace YouAreBaba.ViewModel
{
    public interface IMenuViewModel
    {
        // HIGHSCORE ******************************************************
        int SwitchView { get; set; }
        bool GameViewhasbeenloaded { get; set; }
        BitmapImage Logo { get; }
        BitmapImage Back { get; }
        BitmapImage Help { get; }
        List<int> FromSavedGameFinishedMap { get; set; }
        int LastPlayedMap { get; set; }
        ObservableCollection<FluidButton> MyButtons { get; set; }
        string GamerName { get; set; }
        SolidColorBrush GameBackgroudColor { get; set; }
        IMenuLogic Logic { get; }
        // HIGHSCORE ******************************************************
        void ChangeLastPlayedMapButtonToRed();
        void ChangeLastPlayedMapButtonToGreen();
        void ChangeVictoriousPlayedMapButtonToGreen();
        void ChangeAllPlayedMapButtonToNothing();
        List<int[]> ScanMapSizes();

        // HIGHSCORE ******************************************************
        List<Highscore> Get();

        bool Add(string name, int mapid, int time);

        bool DeleteAllRecord(string name);
        List<int> GetAllVictoryMapNumberByUser(string name);
        bool CheckUserExists(string name);
        void SetGAmeBackgroundColor(int id);
        bool CanLoad { get; set; }
        int PositionHeight { get; set; }
        int PositionWidth { get; set; }
        void SetMapForGameplay(int mapid, int objectPixel);
        // HIGHSCORE ******************************************************
    }
}
