﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;
using YouAreBaba.Logic;
using YouAreBaba.Model;

namespace YouAreBaba.ViewModel
{
    public interface IGameViewModel
    {
        IGameLogic Logic { get; set; }
        List<MapObject> CurrentlyPlayedMap { get; set; }
        List<MapObject> PrevoiusmoveCurrentlyPlayedMap { get; set; }
        List<BitmapImage> MapObjectImages { get; set; }
        DateTime Started { get; set; }
        int CurrentMap { get; set; }
        void AddSaveGame(); 
        void LoadGame();
        int Move(int v1, int v2);
        void MoveSmoothlyPixels(int x, int y, int thismuchmoved);
        List<string> ScanAllPictureNames();
        Rect GetCurrentlyPlayedMapObjectByCoordinates(int i, int j);
        string GetEmumNameOfCurrentlyPlayedMapObjectByCoordinates(int i, int j);
        List<string> ScanAllFramesOfGif(string temp);
    }
}
