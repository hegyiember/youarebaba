﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouAreBaba.ViewModel
{
    public interface IUserViewModel
    {
        string ChosenUser { get; set; }
        List<string> Usernames { get; set; }
        string FromListChosenUser { get; set; }
        bool FromListChosenUserBool { get; set; }
        bool BoolchosenUser { get; set; }
        bool CheckUserExists();
        bool Add(string name, int mapid, int time);
    }
}
